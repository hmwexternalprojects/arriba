<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

 	$ID = get_the_ID();
	$terms = $type === 'product' ? get_the_terms( $ID, 'product_cat' ) : get_the_terms($ID, 'category');
	$firstTerm = $terms[0];
	$termURL = get_category_link($firstTerm);
	$url = get_the_permalink();
	$image = get_the_post_thumbnail_url($ID, 'medium') ? get_the_post_thumbnail_url($ID, 'medium') : null; // From the image ID field type to enable easy image sizes
			
?>
<div class="bmcb-column col-md-12">
	<article data-fade="{x:0}" id="post-<?php the_ID(); ?>" class="search-result mb-4 flex" <?php post_class(); ?>>
		<?php if (isset($image)) { ?>
			<a class="search-result-image-wrapper" href="<?php echo $url; ?>">
				<img src="<?php echo $image; ?>" alt="<?php the_title(); ?>" class="search-result-image" />
			</a>
		<?php } ?>
		<div class="search-result-content">
			<a class="search-result-term-link term-link pb-2 d-block" href="<?php echo $termURL; ?>">
				<?php echo $firstTerm->name; ?>
			</a>
			<h4 class="search-result-title">
				<a href="<?php echo $url; ?>">
					<?php the_title(); ?>
				</a>
			</h4>
			<div class="search-result-content">
				<?php if($type === 'product') {
					if( have_rows('product_content_details') ): while ( have_rows('product_content_details') ): the_row();
						$content = get_sub_field('product_description') ? get_sub_field('product_description') : 'No description';
					endwhile; endif;
					} else {
						$content = get_the_excerpt();
					}
					echo wp_trim_words($content, 30);
				?>
			</div>
				<!-- <a class="view-more-link text-uppercase" href="<?php echo $termURL; ?>"><?php echo $firstTerm->name; ?></a> -->
		</div>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
