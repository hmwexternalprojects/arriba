<div data-fade="{x:0}" class="article-grid__article bmcb-column col-md-6">
  <?php
  		$ID = get_the_ID();
		$index = $query->current_post; 
		$postURL = get_permalink($ID);
		$imageSize = 'article-grid'; 
		$image = get_the_post_thumbnail($ID, $imageSize);
		$cats = get_the_category($ID); 
		$cat = $cats[0];
		echo sprintf("<a class='article-grid__image-wrapper' href='%s'>%s</a>", $postURL, $image); ?>
  <span class="article-grid__term"><?php print_r($cat->name); ?></span>
  <!-- <span class="article-grid__date"><?php the_time('F jS, Y'); ?></span> -->
  <h3 class="article-grid__title">
    <a href="<?php echo $postURL; ?>">
      <?php the_title(); ?>
    </a>
  </h3>
  <a href="<?php echo $postURL; ?>" class="article-grid__icon-link">
    Read more <i class="icon icon-arrow-right text-white"></i>
  </a>
</div>