<div data-fade="{x:0}" class="job-list__job">
  <?php
  	$ID = get_the_ID();
		$index = $query->current_post; 
		$postURL = get_permalink($ID);
		$imageSize = 'job-list'; 
		$image = get_the_post_thumbnail($ID, $imageSize) ? get_the_post_thumbnail($ID, $imageSize) : '/wp-content/uploads/2020/01/f4a25982-5962-3239-9d5c-4386f4c5f8ee.jpg';
		$cats = get_the_category($ID); 
		$cat = $cats[0];
		$job_role = get_the_title();
		$location = get_field('location');
		// echo sprintf("<div class='job-list__image'><a class='job-list__image-wrapper' href='%s'>%s</a></div>", $postURL, $image); 
	?>
  <div class="job-list__content">
    <h3 class="job-list__role"><?php echo $job_role; ?></h3>
    <span class="job-list__location"><?php echo $location; ?></span>
    <?php echo wp_trim_words(get_the_content(), 30); ?>
  </div>
  <div class="job-list__cta">
    <a href="<?php echo $postURL; ?>" class="btn is-outlined text-black bg-red border-red job-list__icon-link">
      Learn more
    </a>
  </div>
</div>