<?php 
	$ID = get_the_ID();
	$position = get_field('position', $ID);
	$title = get_the_title();
	$headshot = get_the_post_thumbnail($ID);
?>
<div class="bmcb-blurb-module bmcb-module person-blurb">
  <?php echo sprintf('<a href="#modal-%s">%s</a>',$ID,$headshot); ?>
  <div class="bmcb-blurb__content person-blurb__content">
    <?php if ($position) : ?>
    <span class="person__position"><?php echo $position; ?></span>
    <?php endif; ?>
    <h3 class="bmcb-blurb__title person-blurb__title"><a
        href="#modal-<?php echo $ID; ?>"><?php echo $title ? $title : ''; ?></a></h3>
  </div>
</div>

<modal classes="modal person__modal" height="80%" width="80%" name="<?php echo $ID; ?>">
  <div class="flex flex-col lg:flex-row h-full">
    <div class="person-modal__content-wrapper" style="max-height: 80vh;">
      <div class="person-modal__content" style="max-height: 80vh;">
        <?php if ($position) : ?>
        <span class="person-modal__position"><?php echo __($position); ?></span>
        <?php endif; ?>
        <h3 class="bmcb-blurb__title person-modal__title"><a
            href="#modal-<?php echo $ID; ?>"><?php echo $title ? $title : ''; ?></a></h3>
        <div class="person-modal__body">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
    <div class="person-modal__headshot">
      <?php echo $headshot; ?>
    </div>
  </div>
</modal>