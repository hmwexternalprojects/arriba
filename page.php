<?php
 get_header();
?>

<div <?php post_class(); ?> id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<?php
				the_content();

				if ( ! $is_page_builder_used )
					wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
			?>

		</div>

	<?php endwhile; ?>
</div> <!-- #main-content -->

<?php

get_footer();
