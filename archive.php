<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<section class="bmcb-section bg-red text-white content-area page-header__hero page-header__hero--small">
				<header class="page-header container">
					<?php
						the_archive_title( '<h1 class="page-title light">', '</h1>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
					?>
				</header>
			</section>

			<section class="bmcb-section container content-area">
				<div class="bmcb-row row">
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						* Include the Post-Type-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Type name) and that will be used instead.
						*/ ?>

						<?php get_template_part( 'template-parts/content-post-grid', get_post_type() ); ?>
							
					<?php endwhile; ?>
				</div>
			</section>

		<?php the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
