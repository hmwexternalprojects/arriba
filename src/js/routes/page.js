import { offsetColumns } from '../util/layout';
import labelMove from '../components/forms/labelMove';

export default {
  init() {
    offsetColumns();
  },
  finalize() {
    labelMove();
  }
};
