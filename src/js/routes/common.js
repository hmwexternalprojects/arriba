// import stickyThang from '../components/stickyThang'
import mainMenu from '../components/mainMenu'
import postFilter from '../components/postFilter'
import accordions from '../components/accordion'
import { enableFadeIn, enableStaggerIn, enableParallax } from "../animations/globalAnimations";
import debounce from '../util/debounce';
import { isAbove } from '../util/breakpoints';

export default {
  init() {

    // push page down the same height as the header
    // (function () {
    //   let stickyPage = document.querySelector('.sticky #page')

    //   if (stickyPage) {
    //     let header = document.getElementById('main-header')
    //     let headerHeight = header.clientHeight

    //     stickyPage.style.paddingTop = `${headerHeight}px`

    //     window.addEventListener('resize', debounce(function () {
    //       headerHeight = header.clientHeight
    //       stickyPage.style.paddingTop = `${headerHeight}px`
    //     }), 50)
    //   }
    // })()

    // Main Menu
    mainMenu();

    // Accordions
    accordions();

    /*
      Enable Fade in animations, you set these in buildy using 
      data attributes: data-fadein : left/right/top/bottom
    */
    enableFadeIn()

    enableParallax()

    /*
      Enable Stagger in animations, you set these in buildy using
      data attributes: data-staggerin : left/right/top/bottom
    */
    enableStaggerIn()

    // First argument is the sticky, second is the anchor (when window scrolls past top-header, main-header sticks)
    // stickyThang('#main-header', '#top-header');
  },
  finalize() {
    (function ($) {
      // Hide Header on on scroll down
      let body = $('body');
      let didScroll;
      let lastScrollTop = 0;
      let delta = 50;
      let navbarHeight = 120;
      let count = 0;
      let interval;

      if (!isAbove.lg.matches) {
        startInterval()
        window.addEventListener('scroll', flagScroll, true);
      }

      isAbove.lg.addListener(e => {
        if (!e.matches) {
          startInterval()
          window.addEventListener('scroll', flagScroll, true);
        } else {
          clearInterval(interval)
          body.removeClass('hide-header').removeClass('show-header')
          window.removeEventListener('scroll', flagScroll, true);
        }
      })

      function flagScroll(event) {
        didScroll = true;
      }

      function startInterval() {
        interval = setInterval(function () {
          if (didScroll) {
            hasScrolled();
            didScroll = false;
          }
        }, 250);
      }


      function hasScrolled() {
        let st = $(window).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
          return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight) {
          // Scroll Down
          console.log('Down')
          count++
          if (count === 3) {
            console.log('More energy')
            count = 0;
          }
          body.removeClass('show-header').addClass('hide-header');
        } else {
          // Scroll Up
          console.log('up')
          if (st + $(window).height() < $(document).height()) {
            body.removeClass('hide-header').addClass('show-header');
          }
        }

        lastScrollTop = st;
      }
    })(jQuery)
    // testiSlider();
    // JavaScript to be fired on the home page, after the init JS
    // const intersectionObserver = new IntersectionObserver((entries, observer) => {
    //   entries.forEach((el) => {
    //     if (el.isIntersecting) {

    //       new NewsModule({
    //         target: el.target,
    //       });

    //       observer.unobserve(el.target);
    //     }
    //   });
    // });

    // const newsModule = document.getElementById('global-news-module')
    // if (newsModule) {
    //   intersectionObserver.observe(newsModule)
    // }
  },
};
