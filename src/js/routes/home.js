import { headerIntro, pinContent, gridParallax } from '../animations/homeAnimations'

export default {
  init() {
    // Trigger animations
    headerIntro()
  },
  finalize() {
    gridParallax()
    pinContent()
  },
};
