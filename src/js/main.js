// /* SVELTE SPECIFIC STUFF */

// import 'core-js/stable'
// import 'objectFitPolyfill'

// // import "@babel/polyfill";
// // import App from './components/App.svelte'
// // import PageContents from './components/PageContents.svelte';
// import SvelteSlider from './components/SvelteSlider.svelte';
// import NewsModule from './components/NewsModule.svelte';
// // import CategorySelect from './components/categorySelect';

// // const pageContents = new PageContents({
// //   target: document.getElementById('page-content-links'),
// // });

// const newsModule = new NewsModule({
//   target: document.getElementById('global-news-module'),
// });

// // const categorySelect = new CategorySelect({
// // 	target: document.getElementById('cat-select'),
// // });
// (function () {
//   const pageSlider = document.getElementById('svelte-slider');
//   const svelteSlider = new SvelteSlider({
//     target: pageSlider,
//     props: {
//       type: "hero",
//       slider: pageSlider && pageSlider.getAttribute('data-slider')
//     }
//   });
// })()

// export { newsModule };
import Vue from 'vue';
import Vue2TouchEvents from 'vue2-touch-events';
import VModal from 'vue-js-modal';

Vue.use(VModal);

// IE 11 Pollyfill --- Object Fit Options
// https://github.com/fregante/object-fit-images#usage
import objectFitImages from 'object-fit-images';
objectFitImages();

Vue.use(Vue2TouchEvents);

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => {
  let name = key
    .split('/')
    .pop()
    .split('.')[0];
  Vue.component(name, files(key).default);
});

// Control which page JS fires on with this router
import Router from './util/Router';

// Import each page / type here
import common from './routes/common';
import page from './routes/page';
// import contactUs from './routes/contact_us';
import home from './routes/home';

/** Populate Router instance with DOM routes */
const routes = new Router({
  common,
  page,
  // contactUs,
  home
});

// Load Events
window.onload = () => {
  routes.loadEvents();
};

let app = new Vue({
  el: '#page',
  mounted() {
    let modalTriggers = document.querySelectorAll('a[href*="#modal-"]');
    let vm = this;
    modalTriggers.forEach(trigger => {
      trigger.addEventListener('click', function (e) {
        let target = this.hash.split('-')[1];
        vm.$modal.show(target);
      });
    });
  }
});
