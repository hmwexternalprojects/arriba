import { ScrollScene } from 'scrollscene'
import { gsap } from "gsap";
// import { cookieHelper } from '../util/lsHelper';
import { isAbove } from '../util/breakpoints'
import { addAnimationFlag, removeAnimationFlag } from './globalAnimations'

// Header intro that happens on the homepage
const headerIntro = () => {

  // Cookie only for Dev, this needs to play every page load
  // let ls = new cookieHelper('header_animation', 24)

  // Add if statement with !ls.checkCookie() if/when cookie needs to be used
  let header = document.getElementById('main-header')
  let mobileToggle = document.querySelector('.mobileToggle')
  let headerContainer = header.querySelector('.header-container')
  let animationContainer = document.getElementById('logo-animation-container')

  // Step 0 Get the canvas el and hide it immediately until ready to go
  let canvas = document.getElementById('canvas')
  canvas.style.display = 'none'

  let logoAnimation = gsap.timeline();
  let scene;

  // Create a new timeline separate to the one attached to scroll (this will auto-play)
  let headerStartAuto = gsap.timeline();

  // Header should be invisible at the start of anumation
  // logoAnimation.to(header, { duration: 1, y: '-120px' }, 0)
  // logoAnimation.to(mobileToggle, { duration: 0.2, opacity: 0 }, 0)

  // Slide event on window object sliderReady 

  // Step 2, slide the main container down to 80% window height
  if (window.pageYOffset < 100) {
    headerStartAuto.to(animationContainer, { duration: 2, height: '80vh', opacity: 1, ease: "expo.inOut", onComplete: stepThree }, 0)
  } else {
    stepThree()
  }

  function stepThree() {
    // Step 3, Get the animation going
    init_logo_animation()

    // Step 3a Also make sure it becomes visible
    canvas.style.display = 'block'

    // Step 4 create the animations of closing the header

    logoAnimation.fromTo(animationContainer, { duration: 3, height: '80vh', opacity: 1, ease: "expo.inOut" }, {
      height: 0, ease: "power4.inOut", onComplete: function () {
        removeAnimationFlag()
        animationContainer.remove();
        // Step 5 bring main-header back into view everytime animation is finished
        let headerScrollDown = gsap.timeline();
        headerScrollDown.to(header, {
          duration: 1,
          y: 0
        }, 1)
        headerScrollDown.to(mobileToggle, { duration: 0.5, opacity: 1 }, 1)

      },
      onStart: function () {
        addAnimationFlag()
      }
    }, 0)

    // Step 4.b
    logoAnimation.to(headerContainer, { duration: 0.3, opacity: 1 }, 0)
    // logoAnimation.to(realLogo, 0.3, { opacity: 1 }, 0)

    // Step 5 Push page padding down the same amount as header height
    // logoAnimation.fromTo(page, 3, { paddingTop: 0}, {
    //   paddingTop: '120px'
    // }, 2)

    // Step 6 Everything from step 4 onwards is only triggered on scrolldown
    scene = new ScrollScene({
      triggerElement: "body",
      triggerHook: 0.2,
      duration: '100%',
      gsap: {
        timeline: logoAnimation,
        yoyo: false
      }
    })

    scene.Scene.on('end', function () {
      scene.Scene.enabled(false);
      (logoAnimation !== 'undefined') ? logoAnimation.kill(true) : null;
      (headerStartAuto !== 'undefined') ? headerStartAuto.kill(true) : null;
    })
    // scene.Scene.addIndicators({ name: 'pin scene', colorEnd: '#FFFFFF' })
  }

  // Not happening at this stage but this will get cookies working for this
  // ls.setCookie()
}

//Custom magazine style grid parallax
const gridParallax = () => {
  let parallaxTween, scene;
  let imageLarge = document.querySelector('.homepage-parallax__image-large')
  let imageSmall = document.querySelector('.homepage-parallax__image-small')
  let textBox = document.querySelector('.homepage-parallax__text-box')

  if (isAbove.lg.matches) {
    parallaxTween = gsap.timeline()
      // .fromTo(".homepage-grid .homepage-grid__intro", 1, {y: 0}, {y: -350, ease: 'none'})
      // .fromTo(".homepage-grid .homepage-grid__image-large", 35, { x: -100 }, { x: 50, ease: '"back.out(2)"' }, 1)
      // .fromTo(".homepage-grid .homepage-grid__image-small", 35, { y: 500, x: -100 }, { y: 0, x: 0, ease: '"back.out(2)"' }, 1)
      // .fromTo(".homepage-grid .homepage-grid__text-box", 35, { y: 200 }, { y: -400, ease: '"back.out(2)"' }, 1).fromTo(".homepage-grid .homepage-grid__image-small", 35, { y: 500, x: -100 }, { y: 0, x: 0, ease: '"back.out(2)"' }, 1)
      .fromTo(imageSmall, { duration: 20, y: 150 }, { y: -100, ease: '"back.out(2)"' }, 1)
      .fromTo(textBox, { duration: 20, y: 400 }, { y: -100, ease: '"back.out(2)"' }, 1)

    scene = new ScrollScene({
      triggerElement: imageLarge,
      triggerHook: 'onEnter',
      duration: '210%',
      gsap: {
        timeline: parallaxTween
      }
    })
    // scene.Scene.addIndicators({ name: 'Parallax scene', colorEnd: '#FFFFFF' })

  } else {
    imageLarge.style.transform = ''
    imageSmall.style.transform = ''
    textBox.style.transform = ''
  }
  isAbove.lg.addListener((e) => {
    if (e.matches) {
      gridParallax()
    } else {
      (scene && scene !== 'undefined') ? scene.Scene.destroy(true) : null;
      (parallaxTween && parallaxTween !== 'undefined') ? parallaxTween.kill(true) : null;

      imageLarge.style.transform = ''
      imageSmall.style.transform = ''
      textBox.style.transform = ''
    }
  })
}

// Section pinning on homepage
const pinContent = () => {
  const page = document.getElementById('page')
  const pinContainer = document.querySelector('.pin-container')
  const pinnedEl = document.querySelector('.pinned-content')
  let scene, pinned;

  const disableScene = () => {
    if (scene) {
      scene.Scene.enabled(false);
      if (pinned) {
        scene.Scene.removePin(pinContainer)
      }
    }
  }

  const enableScene = () => {
    if (scene) {
      scene.Scene.enabled(true);
      if (!pinned) {
        scene.Scene.setPin(pinContainer)
        pinned = true;
      }
    }
  }

  if (!isAbove.lg.matches) {
    // disableScene()
  } else {
    if (!scene) {
      scene = new ScrollScene({
        triggerElement: page,
        triggerHook: 0,
        duration: '200%',
      })
      enableScene()
    }
  }

  isAbove.lg.addListener((e) => {
    if (!e.matches) {
      // disableScene()
    }
    else {
      enableScene()
    }
  })
}


export { headerIntro, gridParallax, pinContent }
