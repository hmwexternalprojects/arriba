import { ScrollScene, ScrollMagic } from 'scrollscene'
import { gsap } from "gsap";
import { isAbove } from '../util/breakpoints'


// Add body animation flag
const addAnimationFlag = () => {
  const body = document.body;
  let addEvt = new CustomEvent('animationFlagAdd');
  if (!body.classList.contains('animating')) {
    body.classList.add('animating')
    window.dispatchEvent(addEvt)
  }
}

// Remove body animation flag
const removeAnimationFlag = () => {
  const body = document.body;
  let removeEvt = new CustomEvent('animationFlagRemove');
  if (body.classList.contains('animating')) {
    body.classList.remove('animating')
    window.dispatchEvent(removeEvt)
  }
}

// The style of the fading animations
const fadeAnimation = (el) => {
  let userOptions = el.getAttribute('data-fade')
  let defaultOptions, faderTween;

  if (el.getAttribute('data-fade') !== 'opacity') {
    userOptions ? userOptions = strToObj(userOptions) : false
    defaultOptions = { x: 0, y: 8, opacity: 0 }
    let combinedOptions = userOptions ? Object.assign(defaultOptions, userOptions) : defaultOptions;

    faderTween = gsap.timeline()
    faderTween.fromTo(el, 1, combinedOptions, { x: 0, y: 0, opacity: 1, ease: '"back.out(2)"' }, 0)

    el.removeAttribute('data-fade')

  } else {
    // Just do a CSS only one for static opacity
    el.classList.add('css-fadein')
  }

}

// The style of the stagger animations
const staggerInAnimation = (el) => {
  let children = el.children
  let userOptions = el.getAttribute('data-staggerin')
  let defaultOptions, staggerTween;

  if (el.getAttribute('data-staggerin') !== 'opacity') {
    userOptions ? userOptions = strToObj(userOptions) : false
    defaultOptions = { x: 0, y: 8, opacity: 0 }
    let combinedOptions = userOptions ? Object.assign(defaultOptions, userOptions) : defaultOptions;
    staggerTween = gsap.timeline()

    staggerTween.fromTo(children, 3, combinedOptions,
      {
        x: 0,
        y: 0,
        opacity: 1,
        ease: 'expo.out',
        stagger: 0.4
      },
      0.4
    );
  } else {
    children.forEach(child => {
      // Staggerin the classes
      setTimeout(() => { child.classList.add('css-fadein') }, 400)
    })
  }

  el.removeAttribute('data-staggerin')
  // This will lock the animation to the scroll position
  // scene = new ScrollScene({
  //   triggerElement: el,
  //   triggerHook: 1,
  //   duration: '90%',
  //   gsap: {
  //     timeline: staggerTween
  //   }
  // })
}

// The style of the parallax animations
const parallaxAnimation = (el) => {
  let userOptionsFrom = el.getAttribute('data-parallax')
  let userOptionsTo = el.getAttribute('data-parallax-to')
  let laxSpeed = el.getAttribute('data-parallax-speed')
  let defaultOptionsFrom, defaultOptionsTo, laxTween, scene;
  let height = el.offsetHeight

  if (isAbove.lg.matches) {
    userOptionsFrom ? userOptionsFrom = strToObj(userOptionsFrom) : false
    defaultOptionsFrom = { y: `${height}px` }

    userOptionsTo ? userOptionsTo = strToObj(userOptionsTo) : false
    defaultOptionsTo = { y: `-${height}px` }

    let combinedOptionsFrom = userOptionsFrom ? Object.assign(defaultOptionsFrom, userOptionsFrom) : defaultOptionsFrom;
    let combinedOptionsTo = userOptionsTo ? Object.assign(defaultOptionsTo, userOptionsTo) : defaultOptionsTo;

    laxTween = gsap.timeline()
    laxTween.fromTo(el, parseInt(laxSpeed) || 500, combinedOptionsFrom, combinedOptionsTo, 1)

    scene = new ScrollScene({
      triggerElement: el,
      triggerHook: 1,
      duration: '200%',
      gsap: {
        timeline: laxTween
      }
    })
  }

  isAbove.lg.addListener((e) => {
    if (e.matches) {
      parallaxAnimation(el)
    } else {
      (scene && scene !== 'undefined') ? scene.Scene.destroy(true) : null;
      (laxTween && laxTween !== 'undefined') ? laxTween.kill(true) : null;

      el.style.transform = ''
    }
  })
}

// The intersection observer to monitor when any animating object is in view
const intersectionObserver = new IntersectionObserver((entries, observer) => {
  entries.forEach((el) => {
    if (el.isIntersecting) {

      const dataAtts = Object.keys(el.target.dataset)

      dataAtts.forEach(att => {
        switch (att) {
          case 'fade':
            fadeAnimation(el.target)
            break
          case 'staggerin':
            staggerInAnimation(el.target)
            break
          case 'parallax':
            parallaxAnimation(el.target)
            break
        }
      })
      observer.unobserve(el.target);
    }
  });

});

// The different animations are split into functions to be able to enable/disable
// These on a per-route basis. To enable across all add them in common.js
const enableFadeIn = () => {
  const fadeInEls = document.querySelectorAll('[data-fade]')

  if (fadeInEls.length) {
    fadeInEls.forEach(el => {
      intersectionObserver.observe(el)
    })
  }
}

// As above
const enableStaggerIn = (lockToScroll) => {
  const staggerEls = document.querySelectorAll('[data-staggerIn]')

  if (staggerEls.length) {
    staggerEls.forEach(el => {
      intersectionObserver.observe(el)
    })
  }
}

const enableParallax = (lockToScroll) => {
  const parallaxes = document.querySelectorAll('[data-parallax]')

  if (parallaxes.length) {
    parallaxes.forEach(el => {
      intersectionObserver.observe(el)
    })
  }
}

function strToObj(str) {
  let obj = {};
  if (str && typeof str === 'string') {
    let objStr = str.match(/\{(.)+\}/g);
    eval("obj =" + objStr);
  }
  return obj
}

export { enableFadeIn, enableStaggerIn, enableParallax, removeAnimationFlag, addAnimationFlag }
