export default function() {
  /*  
    All these required variables can be dropped into your custom loop
    $query is whatever variable you assigned to the query like: $variable = new WP_Query()
      $queryVars = json_encode($query->query_vars, true);
      $current_page = get_query_var('paged');
      $current_page = max( 1, $current_page );
      $total_pages = ceil( $total_rows / $atts['perpage'] );
      
    And here is the code/DIV you would use to show the actual button required a shortcode att of "pagination" === "loadmore"
      <?php if ($atts['pagination'] === 'loadmore') : ?>
        <div class="loadmore-button btn__loader"
          data-queryvars='<?php echo $queryVars ?>'
          data-currentpage="<?php echo $current_page; ?>" 
          data-totalpages="<?php echo $total_pages; ?>"
          data-perpage="<?php echo $atts['perpage']; ?>"
          data-templatepart="post-grid"
          data-container=".article-grid">
          Load more
        </div>
      <?php endif; ?>

    So if you're using the JS routes like JP does in the starter project, this can go only on the route it's loaded
    e.g "page" or "news-page". If you just want it available everywhere, add it to common.js
    
    import loadMore from './path/to/this-file'
    
    finalize() {
      // call it inside finalize
      loadMore()
    }
  */
  let loadMoreBtn = document.querySelector('.post-grid__loadmore')
  if (loadMoreBtn) {
      let queryVars = loadMoreBtn.getAttribute('data-queryvars'),
          currentPage = loadMoreBtn.getAttribute('data-currentpage'),
          totalPages = loadMoreBtn.getAttribute('data-totalpages'),
          template_part = loadMoreBtn.getAttribute('data-templatepart'),
          targetContainer = loadMoreBtn.getAttribute('data-container')
          
      loadMoreBtn.addEventListener('click', function (e) {
        e.preventDefault()

        loadMoreBtn.classList.add('is-loading')

        let data = {
          query: queryVars,
          page: currentPage,
          template_part
        };
        
        if (currentPage !== totalPages) {      
          fetch(`${wp_global_vars.apiBase}/hmw_ajax_load_more`, {
            method: "post",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          })
            .then(res => res.text())
            .then(data => {
              
              loadMoreBtn.setAttribute('data-currentpage', currentPage++)
              
              // return console.log(JSON.parse(data))
              document.querySelector(targetContainer).innerHTML += data

              // Re-trigger fades
              enableFadeIn()

              if (currentPage >= totalPages) { 
                loadMoreBtn.remove()
              } else {
                loadMoreBtn.classList.remove('is-loading')
              }

            }).catch(err => {
              loadMoreBtn.remove()
              console.log(err)
            })
        }
      });
  }
}
