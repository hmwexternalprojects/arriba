import debounce from '../util/debounce'

// Function to stick something to something else. Mostly just used for the sticky nav
export default function(el, anchor, delay = 50, stickyClass = 'sticky') {
  const body = document.querySelector('body')
  
  // Element to stick
  el = document.querySelector(el)

  // Where you want it to stick to
  anchor = document.querySelector(anchor)

  if (!el) {
    return console.log(el + ' not found')
  }

  if (!anchor) {
    return console.log(anchor + ' not found')
  }

  let anchorHeight = anchor.offsetHeight

  window.addEventListener('scroll', debounce(function(e) {
    if (window.scrollY > anchorHeight && !el.classList.contains(stickyClass)) {

      // Add class to the element
      el.classList.add(stickyClass)

      // Add class to body for dom-wide control
      body.classList.add(stickyClass)

    } else if (window.scrollY < anchorHeight) {
      // Remove class from head
      el.classList.remove(stickyClass)

      // Remove class from body
      body.classList.remove(stickyClass)
    }
  }), delay);
}
