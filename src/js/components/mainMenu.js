import debounce from '../util/debounce';
import { isAbove } from '../util/breakpoints';
import { gsap } from "gsap";


export default function () {
  let $ = jQuery;
  let windowWidth = window.innerWidth,
    mainNav = $('#main-nav'),
    header = $('#main-header'),
    body = $('body'),
    mobileToggle = $('.mobileToggle'),
    mobileContainer = $('.mobile-menu'),
    mobileMenuActive = false,
    originalHrefs = [];

  // if (windowWidth < 980 && !mobileMenuActive) {
  // This site has a constant mobile menu
  setupMobileMenu()
  // }        

  // $(window).on('resize', debounce(() => {
  //   windowWidth = window.innerWidth;
  //   if (windowWidth < 980) {
  //     if (!mobileMenuActive) {
  //       setupMobileMenu()
  //     }
  //   } else {
  //     if (mobileMenuActive) {
  //       destroyMobileMenu()
  //     }
  //   }
  // }))

  function setupMobileMenu() {
    mobileMenuActive = true;
    body.addClass('has-mobile-menu')
    let logo = $('#logo').clone().attr('id', 'mobile-logo');
    mobileContainer.append(logo)

    let mobileContainerWidth;

    if (isAbove.lg.matches) {
      mobileContainerWidth = '50vw'
    } else {
      mobileContainerWidth = '100vw'
    }

    isAbove.lg.addListener((e) => {
      if (e.matches) {
        mobileContainerWidth = '50vw'
      } else {
        mobileContainerWidth = '100vw'
      }
      mobileContainer.css({
        transform: `translateX(${mobileContainerWidth})`
      })
    })

    mobileToggle.on('click', function (e) {
      e.preventDefault()
      toggleOpen()
    })
    body.on('click', '.blackout', function (e) {
      e.preventDefault()
      toggleOpen()
    })

    function toggleOpen() {
      mobileToggle.toggleClass('is-open')
      body.toggleClass('mobile-menu-is-visible')
      let menuSlide = gsap.timeline();
      // Add a blackout
      let blackout;
      if (mobileToggle.hasClass('is-open')) {

        menuSlide.to(mobileContainer, 0.6, { x: '0', rotation: 0.01, ease: "Expo.easeInOut" }, 0)

        body.append('<div class="blackout" />')

        menuSlide.to($('.blackout'), 0.4, { opacity: 1 }, 0)

      } else {
        menuSlide.to(mobileContainer, 0.4, {
          x: `${mobileContainerWidth}`, rotation: 0, ease: "Expo.easeInOut", onComplete: function () {
            let closed = new CustomEvent('mobileMenuClosed');
            mobileContainer.get()[0].dispatchEvent(closed)
          }
        }, 0)

        menuSlide.to($('.blackout'), 0.4, {
          opacity: 0, onComplete: function () {
            $('.blackout').remove();
          }
        }, 0)
      }
    }

    setup_collapsible_submenus(toggleOpen);
  }

  function destroyMobileMenu() {
    mobileMenuActive = false;
    body.removeClass('mobile-menu-is-visible')
    remove_collapsible_submenus();
  }

  // This will allow tapping to open nested menus -- Only way for mobile I could think of
  function setup_collapsible_submenus(toggleOpen) {
    mainNav.find('a').each(function (i) {
      $(this).off('click');

      const origURL = $(this).attr('href')

      // If this a tag contains a 
      if ($(this).siblings('.sub-menu').length) {
        // Since we are removing the href, we'll store the original (by index) in an array for later
        originalHrefs.push({
          index: i,
          url: origURL,
        })

        // Remove href
        $(this).attr('href', '#');

        // Otherwise if it has one of these siblings it needs to toggle visible on them
        $(this).on('click', function (event) {
          event.preventDefault();
          if ($(this).siblings('.sub-menu').length) {
            $(this).parent().toggleClass('is-open')
            $(this).siblings('.sub-menu').toggleClass('is-visible')
              .find('.sub-menu')
              .toggleClass('is-visible');
          }
        });
      } else {
        $(this).on('click', function (e) {
          e.preventDefault()
          toggleOpen();
          mobileContainer.on('mobileMenuClosed', function () {
            window.location.href = origURL
          })
        })
      }
    });
  }

  // Reset collapsible states back to normal (so regular clicks work with hover state)
  function remove_collapsible_submenus() {
    // Remove any open items from
    $('.is-visible').removeClass('is-visible');
    mainNav.find('a').each(function (i) {
      $(this).off('click');
      $(this).find('i').remove();

      if ($(this).siblings('.sub-menu').length) {

        let originalHref = originalHrefs.find(function (el) {
          return el.index === i
        })

        $(this).attr('href', originalHref.url);
      }

    });
  }

}
