<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */

get_header();

    // The wordpress way to get search query param
    $s = get_search_query();

    // Check if we are restricting to a particular post type
    $selected_type = isset($_GET['type']) ? $_GET['type'] : null;

    // Base args
    $args = array(
        's' =>$s,
        'posts_per_page' => 4
    );
?>

<section class="bmcb-section bg-red text-white content-area page-header__hero page-header__hero--small">
	<header class="container">
		<h1 class="page-title flex">
			<?php
			/* translators: %s: search query. */
			printf( esc_html__( 'Search Results for: %s', 'hmw' ), '<span class="pl-2">' . get_search_query() . '</span>' );
			?>
		</h1>
		<div>
			<?php get_search_form( ); ?>
		</div>
	</header>
</section>

<!-- Overall page title -->
<!-- <h1 class="search-page-header">Results for <span class="search-term">'echo ucfirst($s);</span></h1> -->

<?php
	// If the URL has a type parameter
	if (!empty($selected_type)) {
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args['paged'] = $paged;
		$args['post_type'] = $selected_type;
	}

	// Separate search results into these types
	$types = array('post', 'careers', 'page');

	// Easy way to map different names to these for showing on frontend (based on design)
	$titleMap = array(
		'post' => 'articles',
		'page' => 'pages'
	);

	// If you want it to use the correct layout (from template_parts) add the map for it here
	$templateMap = array(
		'post' => 'post-grid'
	);

	// Use this to determine if ANY query had a post in it
	$queryCount = 0;

	// This means the URL has a specific type so override array with new type
	// if (isset($paged)) {
	// 	$types = array($selected_type);
	// }

	foreach( $types as $type ) {

		$args['post_type'] = $type;
		$query = new WP_Query( $args );

		if (function_exists('relevanssi_do_query')) {
			relevanssi_do_query( $query );
		}

		if( $query->have_posts() ) {

			// At least 1 post was found in one of the queries
			$queryCount++; ?>

			<!-- This is where it uses the map above to change "post" to "articles" for example -->
			<section class="search-results-container container search-results-container-<?php echo $typeMap[$type]; ?>">
				<div class="bmcb-row row pb-0">
					<h3 class="search-results-section-title pb-2"><?php echo ucfirst($titleMap[$type]); ?></h3>
				</div>
				<div class="bmcb-row row pt-0">			
					<?php while( $query->have_posts()) { 
						$query->the_post(); ?>
						<!-- If the post type matches the type, build the html -->
						<?php if( $type == get_post_type() ){ 
							$template = $templateMap[$type] ? $templateMap[$type] : 'search'
							?>
							<?php get_template_part('template-parts/content-' . $template); ?>
						<?php } ?>
					<?php } /* End while; */ ?>
				</div>
			</section>
	<?php } ?>

	<?php wp_reset_postdata();
	} // End foreach ?>

	<?php
	// If no posts were found inside any query
	if (!$queryCount) { ?>
		<div class="search-no-results">Sorry we couldn't find anything matching <?php echo $s; ?></div>
	<?php }

	?>
<?php
get_footer();
