<?php
 get_header();
?>

<div <?php post_class(); ?> id="main-content">

	<div class="bmcb-section container ">
		<div class="bmcb-row row ">
			<div class="bmcb-column col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
				<div class="bmcb-code-module bmcb-module  ">
					<h1><?php echo do_shortcode('[echo-field field="title"]'); ?></h1>    
				</div>
				<div id="" class="bmcb-code-module bmcb-module  ">
					<?php echo do_shortcode('[service-postcode-checker]'); ?>
				</div>
			</div>
			<div id="" class="bmcb-column col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
				<div id="" class="bmcb-text-module bmcb-module page-links-box ">
					<h3>Page contents</h3><p><a href="link 1" target="_blank">link 1</a></p><p><a href="link 2" target="_blank">link 2</a></p><p><a href="link 3" target="_blank">link 3</a></p><p><a href="link 4" target="_blank">link 4</a></p>
				</div>
			</div>
		</div>
	</div>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<?php
				the_content();

				if ( ! $is_page_builder_used )
					wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
			?>

		</div>

	<?php endwhile; ?>
</div> <!-- #main-content -->

<?php

get_footer();
