const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  context: __dirname,
  entry: {
    frontend: ['./src/js/main.js', './src/sass/style.scss'],
    customizer: './src/customizer.js'
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name]-bundle.js'
  },
  mode: 'development',
  devtool: 'cheap-eval-source-map',
  resolve: {
    // see below for an explanation
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      svelte: path.resolve('node_modules', 'svelte')
    },
    extensions: ['.mjs', '.js', '.svelte'],
    mainFields: ['svelte', 'browser', 'module', 'main']
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: /node_modules/,
        test: /\.jsx$/,
        loader: 'eslint-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader'
      },
      {
        test: /\.s?css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader', {
          loader: 'sass-resources-loader',
          options: {
            resources: [
              path.resolve(__dirname, "./src/sass/functions/_functions-master.scss"),
              path.resolve(__dirname, "./src/sass/variables-site/_colors.scss"),
              path.resolve(__dirname, "./src/sass/variables-site/_structure.scss"),
              path.resolve(__dirname, "./src/sass/mixins/_breakpoints.scss")
            ],
          }
        }, 'postcss-loader']
        //    {
        //   loader: 'postcss-loader', options: {
        //     ident: 'postcss',
        //     plugins: () => [
        //       postcssEnvFunction({
        //         importFrom: [
        //           './css-env-variables.js',
        //         ]
        //       })
        //     ]
        //   }
        // }
      },
      {
        test: /\.(jpe?g|png|gif)\$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images/',
              name: '[name].[ext]'
            }
          },
          'img-loader'
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    //new StyleLintPlugin(),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new BrowserSyncPlugin({
      files: '**/*.php',
      proxy: 'http://arriba.local',
    })
  ],
  optimization: {
    minimizer: [new UglifyJsPlugin(), new OptimizeCssAssetsPlugin()]
  }
};
