<?php
 get_header();
 $cats = get_the_category($ID); 
					$cat = $cats[0];
?>

<div <?php post_class(); ?> id="main-content">

  <div class="bmcb-section container-fluid pt-0">
    <div class="bmcb-row row pt-0">
      <div class="bmcb-column col-xs-12">
        <?php the_post_thumbnail('full-width'); ?>
      </div>
    </div>
  </div>


  <?php while ( have_posts() ) : the_post(); ?>

  <div class="content-wrap">

    <div class="bmcb-section container pt-2">
      <div class="bmcb-row row pt-0">
        <div class="bmcb-column col-xs-12 col-lg-8 ">
          <div class="bmcb-text-module bmcb-module">
            <div class="post-meta post-meta__category">
              <?php echo $cat->name; ?>
            </div>
            <div class="bmcb-code-module bmcb-module  ">
              <h1><?php echo do_shortcode('[echo-field field="title"]'); ?></h1>
            </div>
          </div>
          <div class="bmcb-text-module bmcb-module entry-content">
            <?php
				$ID = get_the_ID();
				$intro = get_field('introduction', $ID) ? get_field('introduction', $ID) : false;

				if ($intro) {
					echo "<p id='post-intro' class='font-large'>$intro</p>";
				} ?>
            <?php the_content(); ?>
          </div>
        </div>
        <div class="bmcb-column col-xs-12 col-lg-4 sidebar">
          <?php get_sidebar(); ?>
        </div>
      </div>
    </div>

  </div>

  <?php endwhile; ?>

  <div class="bmcb-section container ">
    <hr style="height: 4px;" class="bg-lightgray xs:my-1" />
    <div class="bmcb-row row ">
      <div class="bmcb-column col-xs-12">
        <div class="bmcb-code-module bmcb-module">
          <h2 class="text-red lg:text-xxl lg:pb-2">Related</h2>
          <?php
				$categories = get_the_category( get_the_ID() );
				echo do_shortcode('[list-posts cat="'. esc_attr($categories[0]->term_id) .'" perpage="2" pagination="false"]');
			?>
        </div>
      </div>
    </div>
  </div>

</div> <!-- #main-content -->

<?php

get_footer();
