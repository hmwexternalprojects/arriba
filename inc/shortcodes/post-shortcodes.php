<?php

// [testi-slider] You can use any JS slider library you like. Default is Siema
function testi_slider($atts) {
	$atts = shortcode_atts( 
        array(
			'perpage' => 2,
			'image' => true,
			'width' => ''
        ), 
        $atts);

	$args = array(
        'post_type' => 'testimonials',
        'posts_per_page' => $atts['perpage'],
        'post_status' => 'publish'
    );

	$query = new WP_Query($args);

	ob_start();
	
	?>

<div class="my-slider testimonial-slider">
  <?php if ($query->have_posts()) : ?>
  <?php while($query->have_posts()) : $query->the_post(); 
				$ID = get_the_id();
				$width = $atts['width'];
				$imageEnabled = $atts['image'] !== 'false';
				$image = get_the_post_thumbnail_url($ID,'full'); // Returns URL of any custom size (obv)
			
			?>
  <div
    class="testimonial-slider__slide <?php echo ($width) ? "is-$width" : '' ?> <?php echo !$imageEnabled ? 'image-false' : 'image-true'; ?>"
    <?php echo $imageEnabled ? "style=\"background: url('$image')\"" : ''; ?>>
    <div class="container">
      <div class="testimonial-slider__content-box">
        <div class="testimonial-slider__content">
          <div class="testimonial-slider__body"><?php echo __(wp_trim_words(get_the_content(), 40)); ?></div>
          <div class="testimonial-slider__info"><?php echo __("Samantha Smith"); ?></div>
        </div>
        <div class="testimonial-slider__arrow-group">
          <div class="testimonial-slider__arrow arrow-left">
            <i class="fa fa-chevron-left"></i>
          </div>
          <div class="testimonial-slider__arrow arrow-right">
            <i class="fa fa-chevron-right"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endwhile; ?>
  <?php endif; ?>
</div>

<?php
	$output = ob_get_clean();
    wp_reset_postdata();
    return $output;
}

add_shortcode('testi-slider', "testi_slider");

// Enable an article grid with a gravity form in the mix
function article_grid_feature($atts) {
	
	// Category Filter
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts([
		"perpage" => 3,
		"post_type" => 'post',
		"filter" => 'false',
		"taxonomy" => 'category',
		"feature" =>  null,
	], $atts);

	$args = [
		"posts_per_page" => $atts['perpage'],
		"post_status" => "publish",
		"category__in" => $categoryIDs ? [$categoryIDs] : ''
	];

	// if ($categoryIDs) {
	// 	$args['category__in'] = [$categoryIDs];
	// }

	$query = new WP_Query($args); 
	ob_start(); ?>

<?php if($atts['filter'] !== 'false') : ?>
<div class="bg-lightgray xs:p-2 xs:mb-5 post-filter__controls">
  <?php 

				if( $terms = get_terms( array(
					'taxonomy' => $atts['taxonomy'],
					'orderby' => 'name'
				))) : ?>

  <div data-terms='<?php echo json_encode($terms); ?>' id="cat-select"></div>

  <?php 

				endif;
				get_search_form();
				?>
</div>
<?php endif; ?>

<?php 
		if ($query->have_posts()) : 
			$hasFeature = isset($atts['feature']);
			if ($hasFeature) {
				$feature = $atts['feature'];
			}
		?>

<div class="article-grid feature-grid">
  <?php while ($query->have_posts()) : 
				$query->the_post(); 
				$ID = get_the_ID();
				$index = $query->current_post; 
				$postURL = get_permalink();
				$latest = $index === (int) 0; ?>
  <?php if ($latest) : ?>
  <div class="article-grid__latest feature-grid__latest">
    <?php else : ?>
    <div class="article-grid__article feature-grid__article">
      <?php endif; ?>
      <?php
						$imageSize = $latest ? 'article-grid-latest' : 'article-grid'; 
						$image = get_the_post_thumbnail($ID);
						echo sprintf("<a class='article-grid__image-wrapper' href='%s'>%s</a>", $postURL, $image); ?>
      <span class="article-grid__date feature-grid__date"><?php the_time('F jS, Y'); ?></span>
      <h3 class="article-grid__title feature-grid__title">
        <a href="<?php echo $postURL; ?>">
          <?php the_title(); ?>
        </a>
      </h3>
      <a href="<?php echo $postURL; ?>" class="article-grid__icon-link">
        <i class="icon icon-arrow-right text-white"></i>
      </a>
    </div>
    <?php endwhile; ?>



    <?php if ($hasFeature) : ?>
    <div class="feature-grid__feature">
      <?php echo do_shortcode("[" . $feature . "]"); ?>
    </div>
    <?php endif; ?>
  </div>

  <?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
}

add_shortcode('article-grid-feature', 'article_grid_feature');

// This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
add_shortcode('list-posts', function($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts([
		"perpage" => 4,
		"offset" => 0,
		"cols" => 2,
		"cat" => null,
    "post_type" => 'post',
    "pagination" => 'loadmore',
		"paged" => true,
	], $atts );
	
	if (!get_query_var('categories') && isset($atts['cat'])) {
		$categoryIDs = $atts['cat']; 
	}
  
		
	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );

	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	
	$args = [
		"posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => $atts['post_type'],
		"post_status" => "publish",
		"category__in" => $categoryIDs ? [$categoryIDs] : ''
	];

	if ($atts['pagination']) {
		$args['paged'] = $current_page;
	}
	
	$query = new WP_Query($args); 
	$queryVars = json_encode($query->query_vars, true);

	$total_rows = max( 0, $query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $atts['perpage'] );

	ob_start();
	if ($query->have_posts()) : ?>

  <div class="article-grid grid flex-wrap grid-1 grid-lg-<?php echo $atts['cols'];?> gap-3">
    <?php while ($query->have_posts()) : $query->the_post(); ?>
    <?php echo get_template_part( 'template-parts/content-post-grid' ); ?>
    <?php endwhile; 
	?>
  </div>

  <?php if ($atts['pagination'] === 'loadmore') : ?>

  <!-- This is a custom vue component to ajax in more posts of any type -->
  <load-more :query_vars='<?php echo $queryVars; ?>' :current_page="<?php echo $current_page; ?>"
    :total_pages="<?php echo $total_pages; ?>" :perpage="<?php echo $atts['perpage']; ?>" template_part="post-grid"
    container=".article-grid">
  </load-more>
  <?php endif; ?>

  <?php if ($atts['pagination'] === 'paged') : ?>
  <div class="pagination">
    <?php echo paginate_links( array(
					'total'   => $total_pages,
					'current' => $current_page,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
				) ); ?>
  </div>
  <?php endif; 
		// pagination($total_pages) 
		?>

  <?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
});

// This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
add_shortcode('list-jobs', function($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts([
		"perpage" => 4,
		"offset" => 0,
    	"pagination" => 'loadmore',
		"paged" => true,
	], $atts );
		
	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );

	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	
	$args = [
		"posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => 'careers',
		"post_status" => "publish",
	];

	if ($atts['pagination']) {
		$args['paged'] = $current_page;
	}
	
	$query = new WP_Query($args); 
	$queryVars = json_encode($query->query_vars, true);

	$total_rows = max( 0, $query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $atts['perpage'] );

	ob_start();
	if ($query->have_posts()) : ?>

  <div class="job-list__vacancies">
    <?php while ($query->have_posts()) : $query->the_post(); ?>
    <?php echo get_template_part( 'template-parts/content-job-list' ); ?>
    <?php endwhile; 
	?>
  </div>

  <?php if ($atts['pagination'] === 'loadmore') : ?>

  <!-- This is a custom vue component to ajax in more posts of any type -->
  <load-more :query_vars='<?php echo $queryVars; ?>' :current_page="<?php echo $current_page; ?>"
    :total_pages="<?php echo $total_pages; ?>" :perpage="<?php echo $atts['perpage']; ?>" template_part="job-list"
    container=".article-grid">
  </load-more>
  <?php endif; ?>

  <?php if ($atts['pagination'] === 'paged') : ?>
  <div class="pagination">
    <?php echo paginate_links( array(
					'total'   => $total_pages,
					'current' => $current_page,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
				) ); ?>
  </div>
  <?php endif; 
		// pagination($total_pages) 
		?>

  <?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
});

// Echo either Yogi Bear or Boo Boo, Yogi: [echo-field field="content"] | Boo Boo: [echo-field acf_field="anything"]
// More Yogi [echo-field field="post_thumbnail" size="thumbnail"] // params are useful for image sizes
function echo_field_shortcode($atts) {
    if (is_admin()) {return null;}

    extract(shortcode_atts(array(
        'field' => '',
        'acf_field' => '',
        'params' => ''
    ), $atts));

    $ID = get_the_ID();

    $output = false;

    if (!empty($acf_field)) {
        // Check to ensure boo boo is available
        if (function_exists('get_field')) {
            $output = get_field($acf_field);
        }
    }

    // Yogi takes presidence -- make sure field is not set if acf_field is
    if (!empty($field)) {
        $output = 'get_the_' . $field; 
    } 

    // There are inconsistencies with the amount of params in each wordpress function so here is where you adjust 
    // as needed on a case by case... E.G ID needs to come first when doing custom image sizes, but not others
    if ($output && function_exists($output)) {
        switch($output) {
            case 'get_the_post_thumbnail':
                $output = $output($ID, $params);
            break;
            case 'get_the_ID':
                $output = $ID;
            break; 
            default:
                $output = $output();
        }
    }

    return $output ?: 'There were no fields matching your query';
}

add_shortcode('echo-field', 'echo_field_shortcode');