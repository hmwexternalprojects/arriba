<?php 

add_shortcode('child-page-icons', function() {
    if(is_admin()){ return null; }

    $ID = get_the_ID();

    $args = array(
        'post_parent' => $ID,
        'numberposts' => -1,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC'
    );

    $children = get_children( $args );

    if (get_field('additional_icon_blocks', $ID)) {
        foreach(get_field('additional_icon_blocks') as $icon_block) {
            array_push($children, $icon_block);
        }
    }
    
    ob_start();    
    
    ?>

<ul class="child-page-list">

  <?php if ($children) : foreach($children as $child) : ?>

  <li class="child-page hover-grow-sm">
    <div class="child-page__top">
      <?php the_post_thumnail(); ?>
      <i
        class="icon-before icon-<?php echo get_field('page_icon', $child->ID) ? get_field('page_icon', $child->ID) : 'fa-marker'; ?>"></i>
    </div>
    <div class="child-page__body">
      <a href=""><?php echo $child->post_title; ?></span></a>
    </div>
  </li>

  <?php endforeach; endif; ?>

</ul>

<?php
    
    $output = ob_get_clean();
    
    return $output;
});

// The global news module as per homepage (and others)
add_shortcode('global-news-module', function() {
    if(is_admin()){ return null; }    
    ob_start();    
    
    ?>

<div id="global-news-module"></div>

<?php
    
    $output = ob_get_clean();
    
    return $output;
});


// For the buildy feature "Internal Link"
add_shortcode('page-content-links', function() {
	return '<div id="page-content-links"></div>';
});


add_shortcode('awards-list', function($atts) {
    if(is_admin()){ return null; }

	$ID = get_the_ID();
	
	$years = [];

    if (have_rows('awards', $ID)) :
		 while ( have_rows('awards', $ID) ) : the_row();
			if (!$years[get_sub_field('award_year')]) {
				$years[get_sub_field('award_year')] = [];
			}

			array_push($years[get_sub_field('award_year')], [
				'name' => get_sub_field('award_name'),
				'type' => get_sub_field('award_type')
			]);			

        endwhile;
    endif;
    
	ob_start();   ?>

<div data-fade="{x:0}" class="awards-list">
  <?php if(!empty($years)) : foreach($years as $year=>$awards) : ?>

  <div>
    <span class="awards-list__year"><?php echo $year; ?></span>

    <?php if ($awards) : ?>

    <ul class="awards-list__award">

      <?php foreach($awards as $award) : ?>

      <li><?php 
		  echo (!empty($award['type'])) ? "{$award['name']} &ndash; {$award['type']}" : $award['name'];
		?></li>

      <?php endforeach; ?>

    </ul>

    <?php endif; ?>

  </div>

  <?php endforeach; endif; ?>
</div>


<?php
    
    $output = ob_get_clean();
    
    return $output;
});


add_shortcode('logo-grouping', function($atts) {
    if(is_admin()){ return null; }
	ob_start();

if (have_rows('company_logos', 'option')) : ?>
<div class="company-logos">
  <?php while ( have_rows('company_logos', 'option') ) : the_row(); ?>
  <div class="company-logo">
    <?php echo sprintf('<a href="%s">%s</a>', get_sub_field('url'), wp_get_attachment_image(get_sub_field('logo'), 'full')); ?>
  </div>
  <?php endwhile; ?>
</div>
<?php endif;

$output = ob_get_clean();

return $output;

});

add_shortcode('social-links-text', function($atts) {
    if(is_admin()){ return null; }
	ob_start();
	if (have_rows('social_links', 'option')) : ?>
<ul class="social-links">
  <?php while ( have_rows('social_links', 'option') ) : the_row(); ?>
  <li class="social-link">
    <?php 
		$network = get_sub_field('network'); 
		$link = get_sub_field('link')
	?>
    <?php if (isset($network)) : ?>
    <a href="<?php echo $link ? $link : null; ?>"><?php echo $network; ?></a>
    <?php endif; ?>
  </li>
  <?php endwhile; ?>
</ul>
<?php endif;

$output = ob_get_clean();

return $output;

});

add_shortcode('leaders-list', function($atts) {
	if(is_admin()){ return null; }
	
	$args = [
		"posts_per_page" => -1,
		"post_type" => 'leaders',
		"post_status" => "publish"
	];
	
	$query = new WP_Query($args); 
	$queryVars = json_encode($query->query_vars, true);

	ob_start();
	if ($query->have_posts()) : ?>

<div data-staggerin class="person-grid grid flex-wrap grid-1 grid-lg-3 gap-3">
  <?php while ($query->have_posts()) : $query->the_post(); ?>
  <?php get_template_part( 'template-parts/content-leaders' ); ?>
  <?php endwhile; ?>
</div>

<?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
});