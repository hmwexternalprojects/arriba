<?php
// [logo]
function logo_shortcode() {
    if ( is_admin() ){ return null; }

    ob_start();
    echo esc_attr( et_get_option( 'divi_logo' ) );
    return ob_get_clean();
}
add_shortcode('logo', 'logo_shortcode');

// [menu name=""]
function menu_shortcode($atts, $content = null) {
	extract(shortcode_atts(array( 'name' => null, ), $atts));
	$content = '<div class="menu-toggle"><span></span><span></span><span></span></div>';
	$content .= wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
	return $content;
}
add_shortcode('menu', 'menu_shortcode');

// [phone]
function phone_shortcode() {
	if ( is_admin() ){ return null; }
	
	$phoneNumber = get_field('company_phone', 'option');

    ob_start();
    if ( $phoneNumber ) { ?>
        <a href="<?php echo esc_attr( 'tel:' . $phoneNumber ); ?>"><span class="company-phone"><?php echo $phoneNumber; ?></span></a>
    <?php }
    return ob_get_clean();
}
add_shortcode('phone', 'phone_shortcode');

// [email]
function email_shortcode() {
    if ( is_admin() ){ return null; }
	
	$email = get_field('company_email', 'option');

    ob_start();
    if ( $email ) { ?>
        <a href="<?php echo esc_attr( 'mailto:' . $email ); ?>"><span class="company-email"><?php echo $email; ?></span></a>
    <?php }
    return ob_get_clean();
}
add_shortcode('email', 'email_shortcode');

// [year]
function year_shortcode() {
    if ( is_admin() ){ return null; }

    return date('Y');
}
add_shortcode('year', 'year_shortcode');

// [sitename]
function sitename_shortcode() {
    if ( is_admin() ){ return null; }

    return get_option( 'blogname' );
}
add_shortcode('sitename', 'sitename_shortcode');

// [home_url]
function home_url_shortcode() {
    if ( is_admin() ){ return null; }

    return esc_url( home_url( '/' ) );
}
add_shortcode('home_url', 'home_url_shortcode');

// [credits]
function credits_shortcode($atts, $content = null) {
    if ( is_admin() ){ return null; }

    extract(shortcode_atts(array( 'logo' => false, ), $atts));
    $domain_name = preg_replace('/^www\./','',$_SERVER['SERVER_NAME']);
    if( $logo == 'true' ) {
        $html = 'Website by <a href="https://www.handmadeweb.com.au/?utm_source=client_footer&utm_medium=referral&utm_campaign='. $domain_name .'" rel="nofollow" target="_blank"><img src="'. get_stylesheet_directory_uri() .'/images/hmw-logo-white.png" alt="Handmade Web & Design" /></a>';
    } else {
    	$html = 'Website by <a href="https://www.handmadeweb.com.au/?utm_source=client_footer&utm_medium=referral&utm_campaign='. $domain_name .'" rel="nofollow" target="_blank">Handmade Web & Design</a>';
    }
    return $html;
}
add_shortcode('credits', 'credits_shortcode');

function fa_social_shortcode() {
    if(is_admin()){ return null; }

    ob_start();

    if( have_rows('social_icons', 'option') ): 
        
    ?>

        <ul class="fa-social-icons">

            <?php while( have_rows('social_icons', 'option') ): the_row(); ?>

                <li class="fa-social-icon"><a href="<?php the_sub_field('social_network_url'); ?>" class="fab fa-<?php echo strtolower(get_sub_field('social_network')); ?>"></a></li>

            <?php endwhile; ?>

        </ul>

    <?php endif;
    
    $output = ob_get_clean();
    
    return $output;
}

add_shortcode('fa-social-icons', 'fa_social_shortcode');

// [share_post]
function share_post_shortcode($atts) {
    extract(shortcode_atts(array(
    'title' => 'Share'
    ), $atts));
    ob_start();
    if( is_single() || function_exists('is_product') && is_product() ) {
    $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full'); ?>
    <div class="social-share">
        <span class="share-title"><?php echo $title; ?></span>
        <ul>
            <li class="facebook fab fa-facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"></a></li>
            <li class="twitter fab fa-twitter"><a href="https://twitter.com/home?status=<?php echo get_permalink(); ?>" target="_blank"></a></li>
            <li class="pinterest fab fa-pinterest"><a href="https://pinterest.com/pin/create/button/?url=&media=<?php echo $img[0]; ?>&description=" target="_blank"></a></li>
            <li class="email fas fa-envelope"><a href="mailto:?&body=<?php echo get_permalink(); ?>"></a></li>
        </ul>
    </div>
    <?php }
    return ob_get_clean();
}

add_shortcode('share_post', 'share_post_shortcode');
