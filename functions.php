<?php
/**
 * _s functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

 if (is_admin()) {
	 wp_enqueue_editor();
 }

if ( ! function_exists( 'hmw_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hmw_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on _s, use a find and replace
		 * to change 'hmw' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hmw', get_stylesheet_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );


		// Gutenburg
		add_theme_support( 'align-wide' );
		add_theme_support( 'align-full' );

		//Image Sizes 
		add_image_size('article-grid', 700, 440, true);
		add_image_size('headshot', 1400, 1000, true, array( 'center', 'top' ));
		add_image_size('article-grid-latest', 445, 520, true);
		add_image_size('full-width', 3840, 880, true);


		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// Add custom editor css
		add_theme_support( 'editor-styles' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'hmw' ),
			'top-menu' => esc_html__( 'Top Menu', 'hmw' ),
			'footer-menu' => esc_html__( 'Footer Menu', 'hmw' ),
			'footer-menu-bottom' => esc_html__( 'Footer Menu Bottom', 'hmw' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hmw_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 85,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'hmw_setup' );

/* Custom URL Params */
function hmw_query_vars( $qvars ) {
    $qvars[] = 'categories';
    return $qvars;
}
add_filter( 'query_vars', 'hmw_query_vars' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hmw_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'hmw_content_width', 640 );
}
add_action( 'after_setup_theme', 'hmw_content_width', 0 );


function prefix_reset_metabox_positions(){
	delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_post' );
	delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_page' );
	delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_YOUR_CPT_SLUG' );
}

add_action( 'admin_init', 'prefix_reset_metabox_positions' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hmw_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'hmw' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'hmw' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'hmw_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hmw_scripts() {

	// Loads required CSS header only.
	wp_enqueue_style( 'hmw-style', get_stylesheet_uri() );

	wp_register_script( 'hmw-frontend-scripts', get_stylesheet_directory_uri() . '/public/frontend-bundle.js', array('jquery'), null, true );
	
	
	// Loads bundled frontend CSS.
	wp_enqueue_style( 'hmw-frontend-styles', get_stylesheet_directory_uri() . '/public/frontend.css' );
	
	wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true);
	
	wp_enqueue_script('pollyfill', 'https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver%2CCustomEvent%2Cfetch%2CElement.prototype.remove', [], null, false);
	 
	// Only load these dependancies if we need to animate the header logo
	if (is_front_page() && !isset($_COOKIE["header_animation"])) {
		wp_enqueue_script('tweenjs', 'https://code.createjs.com/1.0.0/tweenjs.min.js', array(), null, true);
		wp_enqueue_script('easlejs', 'https://code.createjs.com/1.0.0/easeljs.min.js', array(), null, true);
		wp_enqueue_script('logoAnimation', get_stylesheet_directory_uri() . '/public/logoAnimation.js', array('easlejs','tweenjs'), null, true);
		wp_enqueue_script('logoAnimationInit', get_stylesheet_directory_uri() . '/public/logoAnimationInit.js', array('logoAnimation'), null, true);
	}
	
	if ( is_page( 1013 ) || is_page( 531 ) ) {
		wp_enqueue_script('iframe-resize', 'https://arribagroup.elmotalent.com.au/js/iframeResizer.min.js', array(), null, false);
	}
	
	wp_enqueue_script('hmw-frontend-scripts');
  	wp_localize_script( 'hmw-frontend-scripts', 'wp_global_vars',
        array( 
            'apiBase' => get_rest_url(get_current_blog_id(), 'wp/v2'),
			'postID' => get_the_ID(),
			'admin_ajax_url' => admin_url( 'admin-ajax.php' ),
        )
	);
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hmw_scripts' );

// This is the initial script that puts a class on body before everything renders
// preventing pop-in before animations are ready
function custom_content_after_body_open_tag() {

    ?>

<script>
// Add animating class to body immediately for fadeins/staggerins
document.body.classList.add('animating');
</script>

<?php 
// If we are on the homepage and the animation has not run in the last 24 hours
if (is_front_page() && !isset($_COOKIE["header_animation"])) : 
	echo "<script>
	document.body.classList.add('logoAnimation')
	</script>";
endif; 
}

add_action('wp_body_open', 'custom_content_after_body_open_tag');


add_editor_style( 'editor-style.css' );

/** 
 * Move all Gforms scrips to the bottom of body
 */
add_filter( 'gform_init_scripts_footer', '__return_true' );
/**
 * Implement the Custom Header feature.
 */
require get_stylesheet_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_stylesheet_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_stylesheet_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_stylesheet_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_stylesheet_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_stylesheet_directory() . '/inc/woocommerce.php';
}

/****************************************************************************************************************
* Gravity Forms
*/
// Auto Scroll to Confirmation/Error Message
add_filter( 'gform_confirmation_anchor', '__return_true' );

// Gravity Forms - Hide Label Option
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Gravity Forms - Convert the default button markup from <input /> to a <button /> for custom styling. -- Still uses the settings / button text option
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>{$form['button']['text']}</span></button>";
}
function gform_column_splits( $content, $field, $value, $lead_id, $form_id ) {
	if( !IS_ADMIN ) { // only perform on the front end

		// target section breaks
		if( $field['type'] == 'section' ) {
			$form = RGFormsModel::get_form_meta( $form_id, true );

			// check for the presence of multi-column form classes
			$form_class = explode( ' ', $form['cssClass'] );
			$form_class_matches = array_intersect( $form_class, array( 'two-column', 'three-column' ) );

			// check for the presence of section break column classes
			$field_class = explode( ' ', $field['cssClass'] );
			$field_class_matches = array_intersect( $field_class, array('gform_column') );

			// if field is a column break in a multi-column form, perform the list split
			if( !empty( $form_class_matches ) && !empty( $field_class_matches ) ) { // make sure to target only multi-column forms

				// retrieve the form's field list classes for consistency
				$form = RGFormsModel::add_default_properties( $form );
				$description_class = rgar( $form, 'descriptionPlacement' ) == 'above' ? 'description_above' : 'description_below';

				// close current field's li and ul and begin a new list with the same form field list classes
				return '</li></ul><ul class="gform_fields '.$form['labelPlacement'].' '.$description_class.' '.$field['cssClass'].'"><li class="gfield gsection empty">';

			}
		}
	}
	
	return $content;
}
add_filter( 'gform_field_content', 'gform_column_splits', 10, 5 );

/****************************************************************************************************************
* Include Theme Options // used for global fields (e.g social icons)
*/
include_once dirname(__FILE__) . '/inc/theme_options.php';

/****************************************************************************************************************
* Include Custom REST API Endpoints
*/
include_once dirname(__FILE__) . '/inc/endpoints/loadmore.php';

/****************************************************************************************************************
* Include Custom Shortcodes
*/
include_once dirname(__FILE__) . '/inc/shortcodes/default-shortcodes.php'; // These are the default shortcodes we bundle 
include_once dirname(__FILE__) . '/inc/shortcodes/post-shortcodes.php'; // Everything related to posts
include_once dirname(__FILE__) . '/inc/shortcodes/page-shortcodes.php'; // Everything related to pages

/****************************************************************************************************************
 * No Index No Follow Entire Website - Message
 */
function search_engine_notice() {
  if( get_option('blog_public') === '0' ){
    $notice_class = 'notice notice-error';
    $notice_message = '<h1 style="color: red; padding-top: 0;" class="blink-2">Search Engine Visibility is disabled!</h1>';
    $notice_message .= 'Please enable it <a href="' . site_url() . '/wp-admin/options-reading.php">here</a>';
    $notice_message .= '<style> .blink-2 { animation: blinker 2s linear infinite; } @keyframes blinker { 50% { opacity: 0; } }</style>';
    $notice_message = __($notice_message);

    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $notice_class ), $notice_message);

    unset($notice_class);
    unset($notice_message);
  }
}
function nofollow_meta() {
    echo "<meta name='robots' content='noindex,nofollow' />\n";
}
if( get_option('blog_public') === '0' ) {
    add_action('admin_notices', 'search_engine_notice', 1 );
    add_action( 'wp_head', 'nofollow_meta', 1 );
    add_action( 'login_enqueue_scripts', 'nofollow_meta', 1 );
}

// function add_buildy_to_rest( $data, $post, $context ) {
// 	$buildyContent = get_post_meta( $post->ID, '_BMCB_Content', true );
	
// 	if( $buildyContent ) {
// 		$data->data['buildyContent'] = $buildyContent;
// 	}
	
// 	return $data;
// }
// add_filter( 'rest_prepare_page', 'add_buildy_to_rest', 10, 3 );

/* Pagination */
function pagination($pages = '', $current_page = 1, $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
	
	if(empty($paged)) $paged = 1;

	// If $pages isn't provided, try get it from global wp_query
    if($pages === '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages) {
            $pages = 1;
        }
    }

    if($pages > 1) { 
		ob_start(); ?>

		<div class="pagination">
			<span><?php echo "Page $paged of $pages"; ?></span>

			<?php if($paged > 2 && $paged > $range+1 && $showitems < $pages) : ?>
				<a href='<?php echo get_pagenum_link(1); ?>'>&laquo; First</a>
			<?php endif; ?>

			<?php if($paged > 1 && $showitems < $pages) : ?>
				<a href='<?php echo get_pagenum_link($paged - 1); ?>'>&lsaquo; Previous</a>
			<?php endif; ?>

			<?php for ($i=$current_page; $i <= $pages; $i++) { ?>
				<?php if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) : ?>
					<?php if ($paged == $i) : ?>
						<span class="current"><?php echo $i; ?></span>
					<?php else : ?>
						<a href='<?php echo get_pagenum_link($i); ?>' class="inactive"><?php echo $i; ?></a>
					<?php endif; ?>
				<?php endif; ?>
			<?php } ?>

			<?php if ($paged < $pages && $showitems < $pages) : ?>
				<a href="<?php echo get_pagenum_link($paged + 1); ?>">Next &rsaquo;</a>
			<?php endif; ?>
			<?php if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) : ?>
				<a href='<?php echo get_pagenum_link($pages); ?>'>Last &raquo;</a>
			<?php endif; ?>
		</div>

<?php echo ob_get_clean();
    }
}
