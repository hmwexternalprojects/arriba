<?php
  get_header();
  $ID = get_the_ID();
  $featured_image = get_the_post_thumbnail_url($ID,'full') ? get_the_post_thumbnail_url($ID,'full') : '/wp-content/uploads/2020/03/arriba-leadership-team@2x.jpg';

  $job_role = get_the_title();
	$location = get_field('location');
	$job_adder_url = sanitize_url(get_field('jobadder_url'));

?>

<div <?php post_class(); ?> id="main-content">

  <div class="bmcb-section container-fluid page-header__hero page-header__hero--careers page-header__hero--has-shadow"
    style="background-image: url('<?php echo $featured_image; ?>');">
    <div class="container">
      <div class="bmcb-row row">
        <div class="bmcb-column col-xs-8">
          <div class="bmcb-code-module bmcb-module text-white
            " style="opacity: 1; transform: translate(0px, 0px);">
            Employment
          </div>
          <div class="bmcb-heading-module bmcb-module 
        " style="opacity: 1; transform: translate(0px, 0px);">
            <h1 class="light job-single__page-title">
              <?php echo get_field('page_title_h1') ? get_field('page_title_h1') : 'Current Career <br/> Opportunities'; ?>
            </h1>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php while ( have_posts() ) : the_post(); ?>

  <div class="content-wrap">
    <div class="bmcb-section container job-single">
      <div class="bmcb-row row pb-0 lg:mw-80">
        <div class="bmcb-column col-xs-8">
          <div class="bmcb-code-module bmcb-module">
            <span class="job-single__location"><?php echo $location; ?></span>
            <h2 class="job-single__role"><?php echo $job_role; ?></h2>
          </div>
        </div>
      </div>
      <div class="bmcb-row row lg:mw-80">
        <div class="bmcb-column col-xs-8">
          <div class="bmcb-text-module bmcb-module entry-content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
      <div class="bmcb-row row">
        <div class="bmcb-column col-xs-12">
          <div class="bmcb-code-module bmcb-module">
			  <?php if (isset($job_adder_url)) : ?>
			  <a target="_blank" class="btn bg-red text-white jobadder_button" href="<?= $job_adder_url ?>">Apply Now</a>
			  <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php endwhile; ?>
</div> <!-- #main-content -->

<?php

get_footer();