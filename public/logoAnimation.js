(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {}; var ss = {}; var img = {};
  lib.ssMetadata = [];


  // symbols:
  // helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.Tween5 = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgUAtQgKgDgIgHQgHgFgFgKQgEgKAAgLQAAgKACgGQACgGAFgHQAFgHAGgDQAHgEAIgCIAAAKIgJAEIgJAGQgEAGgCAFQgDAIAAAGQAAAJAEAIQAFAIAGAEQAGAFAJACQAKADAGAAQAIAAAJgDQAKgCAFgFQAHgEAEgIQAEgHAAgKQAAgJgDgHQgCgHgGgFQgGgEgHgDQgHgCgIAAIAAAkIgIAAIAAgtIA1AAIAAAIIgTABQAGADAEAEQAEAFACAEQACACACAIIABALQAAALgEAKQgFAKgHAFQgHAHgLADQgKADgLAAQgKAAgKgDg");
    this.shape.setTransform(0.025, 0);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-5.5, -4.8, 11.1, 9.6);


  (lib.Tween4 = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ag0AqIAAgvQAAgIACgFQABgHAEgEQACgDAGgEQAGgCAHAAQAJAAAIAFQAHAFACAKIAAAAIADgIIAFgFQAEgDADgBIAJgBIAHAAIAPgCIAFgDIAAAMIgDABIgZACQgGABgDACQgFADgDADQgCAFAAAHIAAAlIAvAAIAAAKgAgmgWQgFAFAAAMIAAAlIApAAIAAglQAAgEgBgEQgBgEgDgEQgDgDgEgCQgFgCgDAAQgKAAgGAGg");
    this.shape.setTransform(0.025, 0);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-5.3, -4.2, 10.7, 8.4);


  (lib.Tween3 = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgUAvQgKgDgIgGQgHgHgFgJQgEgKAAgMQAAgLAEgKQAFgKAHgGQAKgGAIgDQAKgDAKAAQALAAAKADQAJADAJAGQAHAGAFAKQAEAKAAALQAAAMgEAKQgFAJgHAHQgHAGgLADQgKADgLAAQgKAAgKgDgAgQglQgIADgHAEQgHAGgEAHQgEAIAAAJQAAAKAEAIQAFAIAGAEQAFAEAKAEQAIACAIAAQAKAAAHgCQAKgEAFgEQAHgEAEgIQAEgGAAgMQAAgKgEgHQgEgHgHgGQgHgEgIgDQgJgCgIAAQgGAAgKACg");
    this.shape.setTransform(0.025, 0);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-5.5, -5, 11.1, 10);


  (lib.Tween2 = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ag1ApIAAgKIBBAAQAKAAAFgCQAHgCAEgEQAFgFABgFQACgFAAgIQAAgHgCgFQgBgFgFgFQgEgEgHgCQgFgCgKAAIhBAAIAAgKIBDAAQAJAAAHACQAJAEADADQAHAGACAHQADAIAAAKQAAALgDAIQgCAHgHAGQgDADgJAEQgHACgJAAg");
    this.shape.setTransform(0.025, 0.025);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-5.4, -4.1, 10.9, 8.3);


  (lib.Tween1 = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ag0AoIAAguQAAgGACgIQACgGAEgEQAEgDAFgDQAHgCAGgBQAHABAGACQAHADACADQADAEADAGQACAFAAAJIAAAkIAtAAIAAAKgAgmgVQgFAGAAALIAAAiIArAAIAAgiQAAgLgFgGQgFgHgMAAQgLAAgFAHg");
    this.shape.setTransform(0.025, 0);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-5.3, -3.9, 10.7, 7.9);


  (lib.R_2 = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_5
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhHAKIAAgUICPAAIAAAUg");
    this.shape.setTransform(7.2, 50.1);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AhHAZIAAgxICPAAIAAAxg");
    this.shape_1.setTransform(7.2, 48.6141);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AhHAoIAAhPICPAAIAABPg");
    this.shape_2.setTransform(7.2, 47.1281);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("AhHA3IAAhtICPAAIAABtg");
    this.shape_3.setTransform(7.2, 45.6422);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#FFFFFF").s().p("AhHBGIAAiLICPAAIAACLg");
    this.shape_4.setTransform(7.2, 44.1563);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#FFFFFF").s().p("AhHBVIAAipICPAAIAACpg");
    this.shape_5.setTransform(7.2, 42.6703);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AhHBjIAAjFICPAAIAADFg");
    this.shape_6.setTransform(7.2, 41.1844);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#FFFFFF").s().p("AhHByIAAjjICPAAIAADjg");
    this.shape_7.setTransform(7.2, 39.6984);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#FFFFFF").s().p("AhHCBIAAkBICPAAIAAEBg");
    this.shape_8.setTransform(7.2, 38.2125);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#FFFFFF").s().p("AhHCQIAAkfICPAAIAAEfg");
    this.shape_9.setTransform(7.2, 36.7266);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#FFFFFF").s().p("AhHCfIAAk9ICPAAIAAE9g");
    this.shape_10.setTransform(7.2, 35.2406);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#FFFFFF").s().p("AhHCuIAAlbICPAAIAAFbg");
    this.shape_11.setTransform(7.2, 33.7547);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#FFFFFF").s().p("AhHC8IAAl3ICPAAIAAF3g");
    this.shape_12.setTransform(7.2, 32.2688);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#FFFFFF").s().p("AhHDLIAAmVICPAAIAAGVg");
    this.shape_13.setTransform(7.2, 30.7828);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#FFFFFF").s().p("AhHDaIAAmzICPAAIAAGzg");
    this.shape_14.setTransform(7.2, 29.2969);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#FFFFFF").s().p("AhHDpIAAnRICPAAIAAHRg");
    this.shape_15.setTransform(7.2, 27.8109);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#FFFFFF").s().p("AhHD4IAAnvICPAAIAAHvg");
    this.shape_16.setTransform(7.2, 26.325);

    this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.shape }] }).to({ state: [{ t: this.shape_1 }] }, 1).to({ state: [{ t: this.shape_2 }] }, 1).to({ state: [{ t: this.shape_3 }] }, 1).to({ state: [{ t: this.shape_4 }] }, 1).to({ state: [{ t: this.shape_5 }] }, 1).to({ state: [{ t: this.shape_6 }] }, 1).to({ state: [{ t: this.shape_7 }] }, 1).to({ state: [{ t: this.shape_8 }] }, 1).to({ state: [{ t: this.shape_9 }] }, 1).to({ state: [{ t: this.shape_10 }] }, 1).to({ state: [{ t: this.shape_11 }] }, 1).to({ state: [{ t: this.shape_12 }] }, 1).to({ state: [{ t: this.shape_13 }] }, 1).to({ state: [{ t: this.shape_14 }] }, 1).to({ state: [{ t: this.shape_15 }] }, 1).to({ state: [{ t: this.shape_16 }] }, 1).wait(24));

    // Layer_2 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_5 = new cjs.Graphics().p("AAAAGQgBAAAAAAQAAAAgBAAQAAgBgBAAQAAAAAAgBIgCgDIAAgBIAAgBIADgDIACAAIAAgBIAEACIACADIgDAFIgCABIgBAAIAAAAg");
    var mask_graphics_6 = new cjs.Graphics().p("AAHBFIgIgCIgMiIQAMABALAEIADABIAACFg");
    var mask_graphics_7 = new cjs.Graphics().p("AAWBFIgJgCIgMiIIAMCIIgNgCIgJAAIgSiFQALgBALgBIAGABQAMABAMAEIADABIAACGg");
    var mask_graphics_8 = new cjs.Graphics().p("AAoBFIgKgCIgLiIIALCIIgMgCIgLAAIgRiFIARCFIgNgBIgOABIgZh8IAHgCQAOgFAOgCQAKgBAMgBIAHABQALABAMAEIAEABIAACGg");
    var mask_graphics_9 = new cjs.Graphics().p("AhCglIAEgDQAUgNATgHIAGgCQAOgFANgCQALgBAMAAIAHAAQALABAMAEIAEABIAACGIgGgCIgJgCIgMiIIAMCIIgNgBIgLgBIgSiFIASCFIgOgBIgOABIgXh8IAXB8QgTABgQAGgAAABAg");
    var mask_graphics_10 = new cjs.Graphics().p("AhMADQAIgNAKgNIAAAAQAQgSASgNIAEgDQAUgNASgGIAHgCQANgFAPgCQALgBALgBIAHABQAMABAMAEIADABIAACGIgGgCIgJgCIgMiIIAMCIIgNgCIgKAAIgSiFIASCFIgPgBIgNABIgZh8IAZB8QgVABgPAGIgfhtIAfBtIgJAEQgXALgOARgAArAwgAhshJIAgAAIAABKg");
    var mask_graphics_11 = new cjs.Graphics().p("Aihg+IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgTATgMIAEgDQAUgNATgGIAGgDQAOgEAOgCQALgCAMAAIAHABQALABAMAEIAEABIAACFIgGgBIgJgCIgMiIIAMCIIgNgCIgLgBIgSiEIASCEIgOAAIgOAAIgYh7IAYB7QgUACgQAGIgfhtIAfBtIgKAEQgWALgPARIgjhUIAjBUIgCAEQgIAKgFANgABfAigAgXgLg");
    var mask_graphics_12 = new cjs.Graphics().p("AihAgIAAh0IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgSATgNIAEgDQAUgNATgGIAGgCQAOgFAOgCQALgBAMgBIAHABQALABAMAEIAEABIAACGIgGgCIgJgCIgMiIIAMCIIgNgCIgLAAIgSiFIASCFIgOgBIgOABIgYh8IAYB8QgUABgQAGIgfhtIAfBtIgKAEQgWALgPARIgjhUIAjBUIgCAEQgIALgFAMIieijICeCjQgFAMgDANIgDASgABfANgAgXghg");
    var mask_graphics_13 = new cjs.Graphics().p("AihBzIAAhsIAAh0IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgSATgNIAEgDQAUgNATgGIAGgCQAOgFAOgCQALgBAMgBIAHABQALABAMAEIAEABIAACHIgGgCIgJgCIgMiJIAMCJIgNgCIgLgBIgSiFIASCFIgOAAIgOAAIgYh8IAYB8QgUACgQAGIgfhuIAfBuIgKAEQgWAKgPARIgjhUIAjBUIgCAEQgIALgFAMIieijICeCjQgFAMgDANIgDASIiThaICTBaQgDAUAAAWIAAAJgABfgMgAgXg6g");
    var mask_graphics_14 = new cjs.Graphics().p("AihCsIAAhTIAAhqIAAh1IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgTATgMIAEgDQAUgNATgHIAGgCQAOgFAOgBQALgCAMAAIAHAAQALABAMAEIAEABIAACHIgGgCIgJgBIgMiKIAMCKIgNgCIgLgBIgSiFIASCFIgOgBIgOABIgYh9IAYB9QgUABgQAGIgfhtIAfBtIgKAFQgWAKgPARIgjhUIAjBUIgCADQgIALgFAMIieiiICeCiQgFAMgDAOIgDASIiThZICTBZQgDATAAAWIAAAJIiQghICQAhIAAAzgABfglgAgXhTg");
    var mask_graphics_15 = new cjs.Graphics().p("AihDFIAAgyIAAhTIAAhqIAAh1IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgTATgMIAEgDQAUgNATgHIAGgCQAOgEAOgCQALgCAMAAIAHAAQALABAMAFIAEABIAACGIgGgBIgJgCIgMiKIAMCKIgNgCIgLgBIgSiFIASCFIgOAAIgOAAIgYh9IAYB9QgUACgQAFIgfhtIAfBtIgKAFQgWALgPARIgjhVIAjBVIgCAEQgIAKgFALIieiiICeCiQgFAMgDAOIgDASIiThZICTBZQgDATAAAWIAAAJIiQghICQAhIAAAzIiQgBICQABIAAAygABfg+gAgXhsg");
    var mask_graphics_16 = new cjs.Graphics().p("AihDeIAAgyIAAgyIAAhTIAAhqIAAh1IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgTATgMIAEgDQAUgNATgHIAGgCQAOgEAOgCQALgCAMAAIAHAAQALABAMAFIAEABIAACGIgGgBIgJgCIgMiKIAMCKIgNgCIgLgBIgSiFIASCFIgOAAIgOAAIgYh9IAYB9QgUACgQAFIgfhtIAfBtIgKAFQgWALgPARIgjhVIAjBVIgCAEQgIAKgFAMIieijICeCjQgFAMgDANIgDASIiThZICTBZQgDATAAAWIAAAJIiQghICQAhIAAAzIiQgBICQABIAAAyIiQgBICQABIAAAygABfhXgAgXiFg");
    var mask_graphics_17 = new cjs.Graphics().p("AihDsIAAgcIAAgyIAAgyIAAhSIAAhrIAAh1IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgTATgMIAEgDQAUgNATgGIAGgDQAOgEAOgCQALgCAMAAIAHABQALABAMAEIAEABIAACGIgGgBIgJgCIgMiJIAMCJIgNgCIgLgBIgSiFIASCFIgOAAIgOAAIgYh8IAYB8QgUACgQAGIgfhuIAfBuIgKAEQgWALgPARIgjhVIAjBVIgCAEQgIAKgFANIieikICeCkQgFALgDAOIgDARIiThZICTBZQgDATAAAWIAAAJIiQggICQAgIAAAzIiQgBICQABIAAAyIiQgBICQABIAAAyIiQgBICQABIAAAcgABfhlgAgXiTg");
    var mask_graphics_18 = new cjs.Graphics().p("AihD3IAAgXIAAgbIAAgyIAAgyIAAhTIAAhqIAAh1IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgTATgMIAEgDQAUgNATgHIAGgCQAOgFAOgBQALgCAMAAIAHAAQALABAMAEIAEABIAACHIgGgCIgJgBIgMiKIAMCKIgNgCIgLgBIgSiFIASCFIgOgBIgOABIgYh9IAYB9QgUABgQAGIgfhtIAfBtIgKAFQgWAKgPASIgjhVIAjBVIgCADQgIALgFAMIieijICeCjQgFAMgDAOIgDASIiThaICTBaQgDASAAAWIAAAJIiQghICQAhIAAAzIiQgBICQABIAAAyIiQgBICQABIAAAyIiQgBICQABIAAAbIiQgBICQABIAAAXgABfhwgAgXieg");
    var mask_graphics_19 = new cjs.Graphics().p("AihEAIAAgRIAAgWIAAgcIAAgyIAAgyIAAhSIAAhrIAAh1IAAgYIBpAAIAhAAIAABLIghhLIAhBLIAAACIAAgBQAHgNALgNIAAAAQAOgSATgNIAEgDQAUgNATgGIAGgCQAOgFAOgCQALgBAMgBIAHABQALABAMAEIAEABIAACHIgGgCIgJgCIgMiJIAMCJIgNgCIgLgBIgSiFIASCFIgOAAIgOAAIgYh8IAYB8QgUACgQAGIgfhuIAfBuIgKAEQgWALgPARIgjhVIAjBVIgCAEQgIALgFAMIieikICeCkQgFAMgDANIgDASIiThaICTBaQgDATAAAWIAAAJIiQghICQAhIAAAyIiQgBICQABIAAAyIiQgBICQABIAAAyIiQgBICQABIAAAcIiQgBICQABIAAAWIiQgBICQABIAAAQgABfh4gAgXimg");

    this.timeline.addTween(cjs.Tween.get(mask).to({ graphics: null, x: 0, y: 0 }).wait(5).to({ graphics: mask_graphics_5, x: 36.05, y: 4.4125 }).wait(1).to({ graphics: mask_graphics_6, x: 31.05, y: 7.0625 }).wait(1).to({ graphics: mask_graphics_7, x: 29.5625, y: 7.05 }).wait(1).to({ graphics: mask_graphics_8, x: 27.8375, y: 7.05 }).wait(1).to({ graphics: mask_graphics_9, x: 25.7, y: 7.1125 }).wait(1).to({ graphics: mask_graphics_10, x: 21.4625, y: 8.75 }).wait(1).to({ graphics: mask_graphics_11, x: 16.2, y: 10.0625 }).wait(1).to({ graphics: mask_graphics_12, x: 16.2, y: 12.25 }).wait(1).to({ graphics: mask_graphics_13, x: 16.2, y: 14.75 }).wait(1).to({ graphics: mask_graphics_14, x: 16.2, y: 17.3 }).wait(1).to({ graphics: mask_graphics_15, x: 16.2, y: 19.7875 }).wait(1).to({ graphics: mask_graphics_16, x: 16.2, y: 22.2875 }).wait(1).to({ graphics: mask_graphics_17, x: 16.2, y: 23.6625 }).wait(1).to({ graphics: mask_graphics_18, x: 16.2, y: 24.8 }).wait(1).to({ graphics: mask_graphics_19, x: 16.2, y: 25.55 }).wait(21));

    // Layer_1
    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#FFFFFF").s().p("AhCiWIgCAEIADgEIABAAQARgVAVgNQAXgPAVgHQAYgIAbAAQAOAAAQAGIAEABIAACGIgGgBQgHgCgPgCIgZgBQgkAAgXAMQgZAMgPAUQgPAUgGAbQgGAaAAAhIAACQQg4kPBCheg");
    this.shape_17.setTransform(22.1296, 21.475);
    this.shape_17._off = true;

    var maskedShapeInstanceList = [this.shape_17];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape_17).wait(5).to({ _off: false }, 0).wait(35));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-30.7, -9.1, 75.2, 69.39999999999999);


  (lib.R = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_3 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().p("AgCAFIgCgCIgBgDIADgEIABgBIADAAIACABIABABIABADIgBACIAAAAIAAABIgCACIgCABIgDgBg");
    var mask_graphics_1 = new cjs.Graphics().p("AhHAMIAAgXICPAAIAAAXg");
    var mask_graphics_2 = new cjs.Graphics().p("AhHAoIAAgXIAAg4ICPAOIAAAqIiPAAICPAAIAAAXgABIARg");
    var mask_graphics_3 = new cjs.Graphics().p("AhHBPIAAgXIAAg4IAAhOICPAeIAAA9IiPgNICPANIAAArIiPAAICPAAIAAAXgABIANg");
    var mask_graphics_4 = new cjs.Graphics().p("AhHCFIAAgYIAAg5IAAhNICPAdIiPgdIAAhrICPBSIAAA2IAAA/IiPgPICPAPIAAAqIiPAAICPAAIAAAYgAhHgZg");
    var mask_graphics_5 = new cjs.Graphics().p("AhHDEIAAgXIAAg6IAAhNIAAhqIAAh/ICPCPIgBAYIAAApIiOhRICOBRIAAA3IiOgeICOAeIAAA+IiOgPICOAPIAAArIiOAAICOAAIAAAXgABHANg");
    var mask_graphics_6 = new cjs.Graphics().p("AhMDwIAAgXIAAg6IAAhNIAAhqIAAh/IAAhYICZC3IgDAMIAAABQgEARgCASIiQiPICQCPIgBAXIAAAqIiPhRICPBRIAAA2IiPgdICPAdIAAA/IiPgPICPAPIAAArIiPAAICPAAIAAAXgABEgIg");
    var mask_graphics_7 = new cjs.Graphics().p("AhaD5IAAgYIAAg5IAAhOIAAhqIAAh+IAAhYIAAgSIBlAAIAjBCIAAAMIADgGIArBQQgHAGgFAHQgMAQgGAUIiYi3ICYC3IgDAMIAAAAQgEARgCASIiPiOICPCOIgBAYIAAApIiOhRICOBRIAAA3IiOgeICOAeIAAA/IiOgPICOAPIAAAqIiOAAICOAAIAAAYgAA+gvg");
    var mask_graphics_8 = new cjs.Graphics().p("AhuD5IAAgYIAAg5IAAhOIAAhqIAAh+IAAhYIAAgSIBmAAIAiAAIAABCIgihCIAiBCIAAAMIADgGQAHgMAIgJIABAAQANgRASgNIAjBsIgLAEQgQAIgMALIgrhQIArBQQgHAGgFAHQgMAQgGAUIiYi3ICYC3IgDAMIAAAAQgEARgCASIiPiOICPCOIgBAYIAAApIiOhRICOBRIAAA3IiOgeICOAeIAAA/IiOgPICOAPIAAAqIiOAAICOAAIAAAYgABIhggAAai2g");
    var mask_graphics_9 = new cjs.Graphics().p("Ah/D6IAAgXIAAg5IAAhOIAAhqIAAh/IAAhYIAAgRIBnAAIAhAAIAABCIghhCIAhBCIAAAMIAEgGQAGgMAJgKIAAAAQANgRATgNIAGgEQATgMATgHIAYB8QgTACgOAFIgjhsIAjBsIgMAFQgQAHgMAMIgqhQIAqBQQgGAGgFAHQgMAQgGATIiZi3ICZC3IgDAMIAAABQgEARgCARIiQiOICQCOIgBAYIAAAqIiPhRICPBRIAAA3IiPgeICPAeIAAA+IiPgOICPAOIAAArIiPAAICPAAIAAAXgABfh2gAAJi0g");
    var mask_graphics_10 = new cjs.Graphics().p("AiJD+IAAgXIAAg5IAAhOIAAhqIAAh/IAAhYIAAgRIBmAAIAjAAIAABCIgjhCIAjBCIAAAMIADgGQAGgMAIgKIABAAQANgRATgNIAGgEQATgMATgHIAXB8IgXh8IAHgDQALgDALgCIAPCEIgGAAIgPAAQgSACgOAFIgjhsIAjBsIgMAFQgQAHgMAMIgqhQIAqBQQgHAGgFAHQgLAQgHATIiYi3ICYC3IgCAMIAAABQgFARgCARIiPiOICPCOIgBAYIAAAqIiOhRICOBRIAAA3IiOgeICOAeIAAA+IiOgOICOAOIAAArIiOAAICOAAIAAAXgAAAiwgABej1g");
    var mask_graphics_11 = new cjs.Graphics().p("AiVEAIAAgYIAAg5IAAhOIAAhqIAAh/IAAhYIAAgRIBmAAIAjAAIAABCIgjhCIAjBCIAAAMIADgGQAHgMAHgKIABAAQAMgRATgMIAGgFQATgMATgHIAIgCQALgEALgCQAOgCAOgBIAJABIACCIIgFgBIgTgBIgPiEIAPCEIgGAAIgPAAIgYh8IAYB8QgSACgPAFIgjhrIAjBrIgLAFQgRAIgMALIgphQIApBQIgLANQgMAQgGAUIiYi4ICYC4IgDALIAAABQgDARgCARIiQiOICQCOIgBAZIAAApIiPhRICPBRIAAA3IiPgeICPAeIAAA+IiPgOICPAOIAAArIiPAAICPAAIAAAYgAgMivg");
    var mask_graphics_12 = new cjs.Graphics().p("AiVEAIAAgYIAAg5IAAhOIAAhqIAAh/IAAhYIAAgRIBmAAIAjAAIAABCIgjhCIAjBCIAAAMIADgGQAHgMAHgKIABAAQAMgRATgMIAGgFQATgMATgHIAIgCQALgEALgCQAOgCAOgBIAJABIACCIIgFgBIgTgBIgPiEIAPCEIgGAAIgPAAIgYh8IAYB8QgSACgPAFIgjhrIAjBrIgLAFQgRAIgMALIgphQIApBQIgLANQgMAQgGAUIiYi4ICYC4IgDALIAAABQgDARgCARIiQiOICQCOIgBAZIAAApIiPhRICPBRIAAA3IiPgeICPAeIAAA+IiPgOICPAOIAAArIiPAAICPAAIAAAYgAgMivg");
    var mask_graphics_13 = new cjs.Graphics().p("AihEAIAAgYIAAg5IAAhOIAAhqIAAh/IAAhYIAAgRIBmAAIAjAAIAABCIgjhCIAjBCIAAAMIADgGQAHgMAJgKIAAAAQAMgRASgMIAHgFQATgMATgHIAHgCQALgEAMgCQANgCAPgBIAJABQAKABALAEIAEABIAACGIgXgEIgCiIIACCIIgGgBIgTgBIgOiEIAOCEIgFAAIgQAAIgXh8IAXB8QgSACgPAFIgjhrIAjBrIgLAFQgQAIgMALIgqhQIAqBQIgLANQgLAQgGAUIiai4ICaC4IgEALIAAABQgEARgBARIiRiOICRCOIgCAZIAAApIiPhRICPBRIAAA3IiPgeICPAeIAAA+IiPgOICPAOIAAArIiPAAICPAAIAAAYgAgYivg");

    this.timeline.addTween(cjs.Tween.get(mask).to({ graphics: mask_graphics_0, x: 3.05, y: 57.125 }).wait(1).to({ graphics: mask_graphics_1, x: 7.225, y: 49.925 }).wait(1).to({ graphics: mask_graphics_2, x: 7.225, y: 47.075 }).wait(1).to({ graphics: mask_graphics_3, x: 7.225, y: 43.175 }).wait(1).to({ graphics: mask_graphics_4, x: 7.225, y: 37.825 }).wait(1).to({ graphics: mask_graphics_5, x: 7.275, y: 31.5 }).wait(1).to({ graphics: mask_graphics_6, x: 7.725, y: 27.1 }).wait(1).to({ graphics: mask_graphics_7, x: 9.2, y: 26.225 }).wait(1).to({ graphics: mask_graphics_8, x: 11.175, y: 26.225 }).wait(1).to({ graphics: mask_graphics_9, x: 12.825, y: 26.075 }).wait(1).to({ graphics: mask_graphics_10, x: 13.85, y: 25.675 }).wait(1).to({ graphics: mask_graphics_11, x: 15.0375, y: 25.55 }).wait(1).to({ graphics: mask_graphics_12, x: 15.0375, y: 25.55 }).wait(1).to({ graphics: mask_graphics_13, x: 16.2, y: 25.55 }).wait(84));

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AihEAIAAnxICJAAIAABOQAIgPALgNIAAAAQAOgTAXgPQAXgPAWgGQAYgJAbAAQAOABAQAFIAEABIAACGIgdgFIgYgBQgkAAgYAMQgYAMgPAUQgPAVgGAaIAAABQgGAcgBAfIAADhg");
    this.shape.setTransform(16.2, 25.55);

    var maskedShapeInstanceList = [this.shape];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(97));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-23.2, -13.5, 59.2, 64.6);


  (lib.b = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_3 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().p("AhHAGIAAgMICPAAIAAAMg");
    var mask_graphics_1 = new cjs.Graphics().p("AhHAjIAAhFICPAAIAABFg");
    var mask_graphics_2 = new cjs.Graphics().p("AhHA/IAAh9ICPAAIAAB9g");
    var mask_graphics_3 = new cjs.Graphics().p("AhHBbIAAi1ICPAAIAAC1g");
    var mask_graphics_4 = new cjs.Graphics().p("AhHB3IAAjtICPAAIAADtg");
    var mask_graphics_5 = new cjs.Graphics().p("AhHCUIAAknICPAAIAAEng");
    var mask_graphics_6 = new cjs.Graphics().p("AhHCwIAAlfICPAAIAAFfg");
    var mask_graphics_7 = new cjs.Graphics().p("AhHDMIAAmXICPAAIAAGXg");
    var mask_graphics_8 = new cjs.Graphics().p("AhHDoIAAnPICPAAIAAHPg");
    var mask_graphics_9 = new cjs.Graphics().p("AhHEFIAAoJICPAAIAAIJg");
    var mask_graphics_10 = new cjs.Graphics().p("AhHEhIAApBICPAAIAAJBg");
    var mask_graphics_11 = new cjs.Graphics().p("AhHE9IAAp5ICPAAIAAJ5g");
    var mask_graphics_12 = new cjs.Graphics().p("AhHFZIAAqxICPAAIAAKxg");

    this.timeline.addTween(cjs.Tween.get(mask).to({ graphics: mask_graphics_0, x: 7.175, y: 68.15 }).wait(1).to({ graphics: mask_graphics_1, x: 7.175, y: 65.3479 }).wait(1).to({ graphics: mask_graphics_2, x: 7.175, y: 62.5458 }).wait(1).to({ graphics: mask_graphics_3, x: 7.175, y: 59.7438 }).wait(1).to({ graphics: mask_graphics_4, x: 7.175, y: 56.9417 }).wait(1).to({ graphics: mask_graphics_5, x: 7.175, y: 54.1396 }).wait(1).to({ graphics: mask_graphics_6, x: 7.175, y: 51.3375 }).wait(1).to({ graphics: mask_graphics_7, x: 7.175, y: 48.5354 }).wait(1).to({ graphics: mask_graphics_8, x: 7.175, y: 45.7333 }).wait(1).to({ graphics: mask_graphics_9, x: 7.175, y: 42.9313 }).wait(1).to({ graphics: mask_graphics_10, x: 7.175, y: 40.1292 }).wait(1).to({ graphics: mask_graphics_11, x: 7.175, y: 37.3271 }).wait(1).to({ graphics: mask_graphics_12, x: 7.175, y: 34.525 }).wait(30));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhHFWIAAqrICPAAIAADxIAAgFIgHG/g");
    this.shape.setTransform(7.175, 34.175);

    var maskedShapeInstanceList = [this.shape];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(42));

    // Layer_7 (mask)
    var mask_1 = new cjs.Shape();
    mask_1._off = true;
    var mask_1_graphics_4 = new cjs.Graphics().p("AABgDIAAABIgBAGg");
    var mask_1_graphics_5 = new cjs.Graphics().p("ABJD3IAAABIgCAHgAA5B7IAKAPIgBADg");
    var mask_1_graphics_6 = new cjs.Graphics().p("AAHD0QgFgagBgZIA/gVIABAXIg1BFIgFgUgABJDtIAAACIgCAGgAA5ByIAKAPIgBADg");
    var mask_1_graphics_7 = new cjs.Graphics().p("AAHD0QgFgagBgZIA/gVIg/AVIAAggIA/ABIAAAJIAAABIABAXIg1BFIgFgUgABJDtIAAACIgCAGgAA5ByIAKAPIgBADg");
    var mask_1_graphics_8 = new cjs.Graphics().p("AAHD0QgFgagBgZIA/gVIg/AVIAAggQABgXAFgWIA6AgIgBAOIg/gBIA/ABIAAAJIAAABIABAXIg1BFIgFgUgABJDtIAAACIgCAGgAA5ByIAKAPIgBADg");
    var mask_1_graphics_9 = new cjs.Graphics().p("AAHD0QgFgagBgZIA/gVIg/AVIAAggQABgXAFgWQAGgfALgeIAhA7IAJASIgJgSIAKAPIgBADIgBAQIg6ggIA6AgIgBAOIg/gBIA/ABIAAAJIAAABIABAXIg1BFIgFgUgABJDtIAAACIgCAGgABBCUg");
    var mask_1_graphics_10 = new cjs.Graphics().p("AAHD0QgFgagBgZIA/gVIg/AVIAAggQABgXAFgWQAGgfALgeQAIgTAKgUIAdBfIgBACIgDAQIgKgPIAJASIgBAQIg6ggIA6AgIgBAOIg/gBIA/ABIAAAJIAAABIABAXIg1BFIgFgUgAA5ByIghg7gABJDtIAAACIgCAGgAA5ByIAKAPIgBADg");
    var mask_1_graphics_11 = new cjs.Graphics().p("AACD0QgDgagBgZIA9gVIg9AVIAAggQABgXADgWQAGgfAMgeQAIgTAKgUQALgVAPgVIAAABIAFgHIAIBwIgCAGQgFAMgEANIgchfIAcBfIAAACIgDAQIgLgPIAKASIgCAQIg6ggIA6AgIAAAOIg+gBIA+ABIgBAJIAAABIABAXIg1BFIgFgUgAA0ByIggg7gABEDtIAAACIgBAGgAA0ByIALAPIgBADgABCBvg");
    var mask_1_graphics_12 = new cjs.Graphics().p("AgYD0QgEgagBgZIA+gVIg+AVIAAggQABgXAEgWQAGgfAMgeQAHgTAKgUQALgVAPgVIAAABIAGgHIALgMIAGgFQARgPAWgIIAFgCIgXByQgMAKgJAPIgKAPIgHhwIAHBwIgCAGQgFAMgEANIgchfIAcBfIAAACIgDAQIgKgPIAJASIgBAQIg6ggIA6AgIgBAOIg+gBIA+ABIAAAJIAAABIABAXIg0BFIgGgUgAAaByIggg7gAApDtIABACIgCAGgAAaByIAKAPIgBADgAAyBQg");
    var mask_1_graphics_13 = new cjs.Graphics().p("AiLCeQgFgagBgZIA/gVIg/AVQgBgQABgPQABgYAFgWQAGgeALgdQAIgUAKgUQAMgWAOgVIAAABIAGgHIALgMIAGgGQARgOAWgIIAFgDIgWB0IAWh0QAogPArAAQAlAAAlAPIhWBlQgPgFgTAAQgbgBgVANIgLAIQgNAJgJAPIgJARIgIhyIAIByIgDAEQgFAMgDANIgdheIAdBeIgBACIgDAQIgKgPIAKARIgCARIg6ggIA6AgIgBAPIg/gBIA/ABIAAAIIAAABIABAXIg1BEIgFgTgAhZAcIghg5gAhJCXIAAABIgCAHgAhZAcIAKAPIAAACgAgLihg");
    var mask_1_graphics_14 = new cjs.Graphics().p("AisCeQgEgagBgZIA+gVIg+AVQgBgQABgPQABgYAEgWQAGgeAMgdQAHgUALgUQALgWAPgVIAAABIAFgHIAMgMIAGgGQARgOAVgIIAGgDQApgPAqAAQAlAAAlAPIAAAAQAlAQAcAdIhxBTQgLgMgNgIIgOgHIBWhlIhWBlQgQgFgSAAQgbgBgVANIgMAIIAXh0IgXB0QgMAJgKAPIgJARIgIhyIAIByIgCAEQgFAMgEANIgcheIAcBeIAAACIgDAQIgLgPIAKARIgCARIg6ggIA6AgIAAAPIg/gBIA/ABIgBAIIAAABIABAXIg1BEIgFgTgAh6AcIggg5gAhqCXIAAABIgBAHgAh6AcIALAPIgBACg");
    var mask_1_graphics_15 = new cjs.Graphics().p("AjFCeQgFgagBgZIA/gVIg/AVQgBgQABgPQABgYAFgWQAGgeALgdQAIgUAKgUQAMgWAOgVIAAABIAGgHIALgMIAGgGQARgOAWgIIAFgDQApgPAqAAQAlAAAlAPIABAAQAmARAcAfQAdAeATAzIABADIiIAtQgGgUgLgRQgOgUgUgMIgPgHIBWhlIhWBlQgOgFgTAAQgcgBgVANIgLAIIAWh0IgWB0QgNAJgJAPIgJARIgIhyIAIByIgDAEQgFAMgDANIgdheIAdBeIgBACIgDAQIgKgPIAKARIgCARIg6ggIA6AgIgBAPIg/gBIA/ABIAAAIIAAABIABAXIg1BEIgFgTgAiTAcIghg5gAiDCXIAAABIgCAHgAiTAcIAKAPIAAACg");
    var mask_1_graphics_16 = new cjs.Graphics().p("AjNCeQgEgagBgZIA+gVIg+AVQgBgQABgPQABgYAEgWQAGgeAMgdQAHgUALgUQALgWAPgVIAAABIAFgHIAMgMIAGgGQARgOAVgIIAGgDQApgPAqAAQAlAAAlAPIAAAAQAmARAdAfQAdAeATAzIABADQALAgAEAoIiQAOQgBgPgEgPIAAgBIgDgKICJgtIiJAtQgGgUgLgRQgOgUgUgMIgNgHIBVhlIhVBlQgQgFgTAAQgbgBgVANIgMAIIAXh0IgXB0QgMAJgKAPIgJARIgIhyIAIByIgCAEQgFAMgEANIgcheIAcBeIAAACIgDAQIgLgPIAKARIgCARIg6ggIA6AgIAAAPIg/gBIA/ABIgBAIIAAABIABAXIg1BEIgFgTgAibAcIggg5gAiLCXIAAABIgBAHgAibAcIALAPIgBACg");
    var mask_1_graphics_17 = new cjs.Graphics().p("AA/CKQAFgVABgVICPAPIiPgPIAAgMQAAgegHgcIAAgBIgCgKICIgtIiIAtQgHgUgLgRQgNgUgVgMIgNgHIBVhlIhVBlQgQgGgSAAQgcAAgVANIgMAIIAXh0IgXB0QgMAJgJAPIgJAQIgIhxIAIBxIgDAGQgFAKgEAOIgcheIAcBeIAAACIgDAQIgKgPIAJARIgBARIg7ghIA7AhIgBAOIg/gBIA/ABIAAAJIAAABIABAXIg1BEIgGgTQgEgagBgZIA/gVIg/AVQgBgQABgQQABgXAEgXQAHgdALgeQAHgUALgTQAMgWAOgWIAAABIAGgGIALgMIAGgGQARgOAVgJIAGgCQApgQAqAAQAlAAAlAQIAAAAQAnARAcAeQAdAfATAzIABADQARAwAABBIgBAcQgCAlgIAfgAibAbIghg6gAiLCWIAAABIgCAHgABFBggAibAbIAKAPIgBACg");
    var mask_1_graphics_18 = new cjs.Graphics().p("AAxCOQAIgRAFgUIAAAAQAGgXABgWIAAgNQAAgdgHgbIAAgBIgCgKQgHgVgLgRQgNgVgVgLIgNgIIBVhkIhVBkQgQgFgSAAQgcAAgVANIgMAIIAXh0IgXB0QgMAJgJAPIgJAQIgIhxIAIBxIgDAGQgFALgEAOIgchgIAcBgIAAACIgDAPIgKgPIAJARIgBARIg7ggIA7AgIgBAOIg/gBIA/ABIAAAIIAAACIABAWIg1BFIgGgUQgEgZgBgZIA/gVIg/AVQgBgQABgQQABgYAEgVQAHgfALgdQAHgUALgUQAMgWAOgVIAAABIAGgGIALgNIAGgFQARgPAVgIIAGgCQApgQAqAAQAlAAAlAQIAAAAQAnAQAcAfQAdAeATAzIABAEIiIAuICIguQARAwAABAIgBAcIiPgOICPAOQgDAzgOAnQgKAdgPAWgAibgJIghg6gAiLByIAAABIgCAHgADUBKgAibgJIAKAPIgBACgADEhCIAAAAg");
    var mask_1_graphics_19 = new cjs.Graphics().p("AgPCZQANgDAKgHQAVgMANgVIAHgLQAIgRAFgUIAAAAQAGgXABgXIAAgMQAAgcgHgdIAAAAIgCgLICIguIiIAuQgHgUgLgRQgNgVgVgMIgNgHIBVhlIhVBlQgQgFgSAAQgcAAgVAMIgMAIIAXhzIgXBzQgMAKgJAPIgJAQIgIhyIAIByIgDAFQgFAMgEANIgchfIAcBfIAAACIgDAQIgKgPIAJASIgBAQIg7ggIA7AgIgBAPIg/gBIA/ABIAAAHIAAACIABAWIg1BFIgGgUQgEgagBgZIA/gUIg/AUQgBgQABgOQABgYAEgWQAHgfALgdQAHgUALgUQAMgWAOgVIAAABIAGgHIALgMIAGgFQARgPAVgIIAGgCQApgQAqAAQAlAAAlAPIAAAAQAnARAcAfQAdAeATAzIABADQARAxAABAIgBAcIiPgPICPAPQgDAzgOAnQgKAcgPAXIh5hJIB5BJQgLARgMANQgeAgglAPIgZAJgAibg0Ighg6gAiLBHIAAABIgCAHgADUAfgAibg0IAKAPIgBADg");
    var mask_1_graphics_20 = new cjs.Graphics().p("AgFFYIgkhxQARAHAVAAQAMAAALgCQAOgDALgHQAVgMANgVIAGgLQAJgRAFgUIAAAAQAFgXACgXIAAgMQAAgdgHgdIAAAAIgDgLQgGgTgLgRQgOgVgUgMQgHgEgHgDIBWhlIhWBlQgQgFgSAAQgbAAgVAMIgMAIIAXhzIgXBzQgMAKgKAPIgJAPIgIhxIAIBxIgDAFQgEAMgEANIgcheIAcBeIAAACIgDAQIgLgPIAKASIgCAQIg6ggIA6AgIgBAOIg+gBIA+ABIAAAJIAAABIABAXIg1BFIgFgUQgEgagCgZIA/gVIg/AVIABggQABgXAEgWQAGgfAMgdQAHgTALgUQALgWAPgVIAAABIAFgHIAMgMIAGgFQARgPAVgIIAGgCQAogQArAAQAlAAAlAPIAAAAQAmARAdAfQAdAeATAzIABADIiJAtICJgtQAQAwAABBIAAAcIiPgPICPAPQgDAzgOAnQgLAcgOAXIh6hJIB6BJQgLARgMANQgeAgglAPQgMAFgNAEIhIhpIBIBpQgYAHgZAAQgaAAgWgEgAh3AeIggg6gAhnCZIAAACIgBAGgAD5BygAh3AeIALAPIgBADgADpgbIAAAAg");
    var mask_1_graphics_21 = new cjs.Graphics().p("AgFFYIgkhxIAkBxQgagEgXgJIgBAAQgKgEgJgGIgIh9QAMATATALIAKAFQARAHAVAAQAMAAALgCQAOgDALgHQAVgMANgVIAGgLQAJgRAFgUIAAAAQAFgXACgXIAAgMQAAgdgHgdIAAAAIgDgLQgGgTgLgRQgOgVgUgMQgHgEgHgDIBWhlIhWBlQgQgFgSAAQgbAAgVAMIgMAIIAXhzIgXBzQgMAKgKAPIgJAPIgIhxIAIBxIgDAFQgEAMgEANIgcheIAcBeIAAACIgDAQIgLgPIAKASIgCAQIg6ggIA6AgIgBAOIg+gBIA+ABIAAAJIAAABIABAXIg1BFIgFgUQgEgagCgZIA/gVIg/AVIABggQABgXAEgWQAGgfAMgdQAHgTALgUQALgWAPgVIAAABIAFgHIAMgMIAGgFQARgPAVgIIAGgCQAogQArAAQAlAAAlAPIAAAAQAmARAdAfQAdAeATAzIABADIiJAtICJgtQAQAwAABBIAAAcIiPgPICPAPQgDAzgOAnQgLAcgOAXIh6hJIB6BJQgLARgMANQgeAgglAPQgMAFgNAEIhIhpIBIBpQgYAHgZAAQgaAAgWgEgAh3AeIggg6gAhnCZIAAACIgBAGgAD5BygAh3AeIALAPIgBADgADpgbIAAAAg");
    var mask_1_graphics_22 = new cjs.Graphics().p("AgFFYIgkhxIAkBxQgagEgXgJIgBAAQgKgEgJgGIgIh9IAIB9IgHgFQgJgGgHgHQgJgJgNgOIAFAHIgHgKIARh0IABgGQAGAVAMARIADADQAMATATALIAKAFQARAHAVAAQAMAAALgCQAOgDALgHQAVgMANgVIAGgLQAJgRAFgUIAAAAQAFgXACgXIAAgMQAAgdgHgdIAAAAIgDgLQgGgTgLgRQgOgVgUgMQgHgEgHgDIBWhlIhWBlQgQgFgSAAQgbAAgVAMIgMAIIAXhzIgXBzQgMAKgKAPIgJAPIgIhxIAIBxIgDAFQgEAMgEANIgcheIAcBeIAAACIgDAQIgLgPIAKASIgCAQIg6ggIA6AgIgBAOIg+gBIA+ABIAAAJIAAABIABAXIg1BFIgFgUQgEgagCgZIA/gVIg/AVIABggQABgXAEgWQAGgfAMgdQAHgTALgUQALgWAPgVIAAABIAFgHIAMgMIAGgFQARgPAVgIIAGgCQAogQArAAQAlAAAlAPIAAAAQAmARAdAfQAdAeATAzIABADIiJAtICJgtQAQAwAABBIAAAcIiPgPICPAPQgDAzgOAnQgLAcgOAXIh6hJIB6BJQgLARgMANQgeAgglAPQgMAFgNAEIhIhpIBIBpQgYAHgZAAQgaAAgWgEgAh3AeIggg6gAhnCZIAAACIgBAGgAhnCbIAAAAgAD5BygAh3AeIALAPIgBADgADpgbIAAAAg");
    var mask_1_graphics_23 = new cjs.Graphics().p("AgpECIgkhxIAkBxQgbgEgXgJIAAAAQgKgEgJgGIgJh8IAJB8IgIgFIgQgMIgVgZIAEAIIgHgKIARh0IgRB0QgagjgQg/IA1hEIg1BEIgGgTQgEgagBgZIA/gVIg/AVQgBgQABgOQABgYAEgWQAHgfALgdQAHgUALgUQAMgWAOgVIAAABIAGgHIALgMIAGgGQARgOAVgIIAGgDQApgPAqAAQAlAAAlAPIAAAAQAnARAcAfQAdAeATAzIABADQARAxAABAIgBAcQgDAzgOAnQgKAcgPAWIh5hJIB5BJQgLASgMANQgeAfglAQIgZAJIhGhqIBGBqQgYAGgZABQgYgBgXgDgAiLBEQAFAWANARIACAEQANASASALIALAFQAQAHAWAAQANAAALgDQANgDAKgGQAVgMANgVIAHgMQAIgQAFgVIAAAAQAGgWABgXICPAPIiPgPIAAgMQAAgdgHgcIAAgBIgCgKQgHgVgLgRQgNgUgVgMIgNgHIBVhlIhVBlQgQgFgSAAQgcgBgVANIgMAIQgMAJgJAPIgJARIgDAFQgFAMgEANIgchfIAcBfIAAACIgDAQIgKgPIAJARIgBARIg7ggIA7AgIgBAPIg/gBIA/ABIAAAHIAAABIABAXQABARAEAQIADAJIgCAIIACgHIgCAHIACgIgAibg3Ighg6gAA8hDICIgugAiChYIgIhygAhkiBIAXh0gACqCogAiLBDgABFANgAibg3IAKAPIgBACg");

    this.timeline.addTween(cjs.Tween.get(mask_1).to({ graphics: null, x: 0, y: 0 }).wait(4).to({ graphics: mask_1_graphics_4, x: 14.475, y: 50.55 }).wait(1).to({ graphics: mask_1_graphics_5, x: 7.275, y: 25.4625 }).wait(1).to({ graphics: mask_1_graphics_6, x: 7.275, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_7, x: 7.275, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_8, x: 7.275, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_9, x: 7.275, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_10, x: 7.275, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_11, x: 7.7125, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_12, x: 10.4, y: 26.375 }).wait(1).to({ graphics: mask_1_graphics_13, x: 21.975, y: 35 }).wait(1).to({ graphics: mask_1_graphics_14, x: 25.225, y: 35 }).wait(1).to({ graphics: mask_1_graphics_15, x: 27.775, y: 35 }).wait(1).to({ graphics: mask_1_graphics_16, x: 28.5125, y: 35 }).wait(1).to({ graphics: mask_1_graphics_17, x: 28.6, y: 35.1375 }).wait(1).to({ graphics: mask_1_graphics_18, x: 28.6, y: 38.75 }).wait(1).to({ graphics: mask_1_graphics_19, x: 28.6, y: 43.075 }).wait(1).to({ graphics: mask_1_graphics_20, x: 24.925, y: 34.775 }).wait(1).to({ graphics: mask_1_graphics_21, x: 24.925, y: 34.775 }).wait(1).to({ graphics: mask_1_graphics_22, x: 24.925, y: 34.775 }).wait(1).to({ graphics: mask_1_graphics_23, x: 28.6, y: 43.4 }).wait(19));

    // Layer_8
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AhbD1IgBAAQgOgGgMgJQgIgGgIgHQgJgJgIgLIAAAEQgjgqgUhVQgaiOBYiAIAAABQAIgLAJgIIAGgGQARgOAVgIQAsgTAtABQAlAAAlAPQAmARAdAfQAdAeATAzQARAyAABCQAABEgRAyQgTAxgdAgQgeAfglAQQglAQglAAQg2AAgrgRgAhYiJQgUALgOAVQgOAVgGAcQgHAbAAAeQAAAfAHAaQAGAcAOAVQAOAVAUAMQAVAMAbAAQAdAAATgMQAVgMANgVQAOgWAGgbIAAAAQAHgdAAgcQAAgdgHgcIAAgBQgGgagOgWQgOgUgUgMQgTgNgdABQgbgBgVANg");
    this.shape_1.setTransform(28.6009, 43.4);
    this.shape_1._off = true;

    var maskedShapeInstanceList = [this.shape_1];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4).to({ _off: false }, 0).wait(38));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-21.6, 0, 78.1, 76.9);


  (lib.a_ = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_9 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_8 = new cjs.Graphics().p("AgCAFIgCgCIgBgDIAAAAIABgDIADgCIACAAIACABIACACQACADgEAEIgCABg");
    var mask_graphics_9 = new cjs.Graphics().p("AgjAHIAOgOIAjgkQAOAWAIAXIhHAqg");
    var mask_graphics_10 = new cjs.Graphics().p("AgpA4QACgQAAgNIAAgDIAAgkIAOgOIAjgkQANAWAJAYQAKAeABAkIhVANIABgHgAgnAYIBHgog");
    var mask_graphics_11 = new cjs.Graphics().p("Ag1BBQAHgHAEgIQAGgLADgKIACgHIABgHQACgPAAgNIAAgDIAAglIAOgOIAjgkQANAVAJAYQAKAgABAjIAAAJQgBA0gZAigAgfAWIBVgNgAgcgQIBHgqg");
    var mask_graphics_12 = new cjs.Graphics().p("AhKA5QANgDAJgFQAMgHAIgJQAHgGAEgIQAGgLADgKIACgHIABgHQACgQAAgNIAAgDIAAglIANgOIAjgkQAOAWAIAYQALAfABAkIAAAJQgBAzgZAjIhRgoIBRAoQgPAUgYAPIABgBIADAAIgDABIgDADQgCADgYAMIgXAKgAgKgJIBVgNgAgHgwIBGgpgAAxBJg");
    var mask_graphics_13 = new cjs.Graphics().p("AhQAxIAEAAQAVAAAPgDQANgDAKgGQALgHAHgIQAHgHAEgIQAGgKADgLIACgHIABgGQACgQAAgOIAAgCIAAglIAOgOIAkgkQAOAVAIAYQAKAgABAkIAAAJQgBAzgZAiIhSgnIBSAnQgPAVgYAOIABAAIADgBIgDACIgDADQgCACgZAMIgWAKIghhOIAhBOIgdALQgiAKgmACgAAXgVIBWgMgAAag7IBIgqgAgHB8gABTA9g");
    var mask_graphics_14 = new cjs.Graphics().p("AiMCMIgBAAIgTgHIBchaIAOAEQAOACAMAAIAEAAQAVAAAOgEQANgDAKgFQALgHAIgIQAHgHAEgIQAGgKADgLIACgHIABgGQACgQAAgOIAAgCIAAglIAOgPIAkgkQAOAWAIAYQAKAgABAjIAAAJQgBAzgZAjIhSgnIBSAnQgPAUgYAPIABAAIADgBIgDABIgDADQgCADgZAMIgXAKIghhPIAhBPIgdALQghAKgmACIAchiIgcBiIgSABQgkAAgegIgABLgVIBWgNgABOg7IBIgqgAAsB8gACHA9g");
    var mask_graphics_15 = new cjs.Graphics().p("AhkCMIAAAAIgTgHIBchaIhcBaQgUgJgPgMQgYgTgNgcIAAAAQgGgNgEgRICOgeQABAIAEAHIAAAAQAFALALAHIgBAAIAMAFIANAEQAOACAMAAIADAAQAVAAAQgEQANgDAJgFQAMgHAIgIQAGgHAFgIQAGgKADgLIACgHIABgGQACgQAAgOIAAgCIAAglIAOgPIAjgkQAOAWAIAYQALAgABAjIAAAJQgBAzgZAjIhSgnIBSAnQgPAUgYAPIABAAIADgBIgDABIgDADQgCADgZAMIgXAKIghhPIAhBPIgdALQgjAKgkACIAbhiIgbBiIgSABQgkAAgfgIgAB0gVIBWgNgAB3g7IBHgqgABVB8gACwA9g");
    var mask_graphics_16 = new cjs.Graphics().p("AhiCMIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgRICNgeIiNAeQgDgRAAgTQAAgsAPgbIAAAAQAFgKAGgIICAAuIgEAIQgHAMAAARIABAMQACAIAEAHIAAAAQAFALALAHIgBAAIAMAFIANAEQANACAMAAIAEAAQAVAAAQgEQANgDAJgFQAMgHAIgIQAGgHAFgIQAGgKADgLIACgHIABgGQACgQAAgOIAAgCIAAglIAOgPIAjgkQAOAWAIAYQALAgAAAjIAAAJQgBAzgZAjIhRgnIBRAnQgPAUgXAPIABAAIADgBIgEABIgCADQgCADgZAMIgXAKIghhPIAhBPIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAB2gVIBVgNgAB5g7IBHgqgABXB8gACxA9g");
    var mask_graphics_17 = new cjs.Graphics().p("AhiCMIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgRICNgeIiNAeQgDgRAAgTQAAgsAPgbIAAAAQAFgKAGgIQAMgQAQgLIAAAAQATgNAYgIIBVBKQgKAFgHAFQgGAEgFAGIiAguICAAuIgEAIQgHAMAAARIABAMQACAIAEAHIAAAAQAFALALAHIgBAAIAMAFIANAEQANACAMAAIAEAAQAVAAAQgEQANgDAJgFQAMgHAIgIQAGgHAFgIQAGgKADgLIACgHIABgGQACgQAAgOIAAgCIAAglIAOgPIAjgkQAOAWAIAYQALAgAAAjIAAAJQgBAzgZAjIhRgnIBRAnQgPAUgXAPIABAAIADgBIgEABIgCADQgCADgZAMIgXAKIghhPIAhBPIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAB2gVIBVgNgAB5g7IBHgqgABXB8gACxA9gAgwgsg");
    var mask_graphics_18 = new cjs.Graphics().p("AhiCPIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgQICNgeIiNAeQgDgSAAgUQAAgrAPgbIAAAAQAFgJAGgJQAMgPAQgMIAAAAQATgNAYgIIAMgDQAdgIAXgEIAuBSIgSAFIABAAIgIADIhVhLIBVBLQgKAEgHAFQgGAFgFAFIiAguICAAuIgEAIQgHAMAAARIABANQACAHAEAHIAAAAQAFALALAHIgBAAIAMAFIANAEQANACAMAAIAEAAQAVAAAQgDQANgDAJgGQAMgHAIgIQAGgHAFgIQAGgKADgLIACgHIABgGQACgQAAgOIAAgCIAAglIAOgOIAjgkQAOAVAIAYQALAgAAAkIAAAJQgBAzgZAiIhRgnIBRAnQgPAVgXAOIABAAIADgBIgEACIgCADQgCACgZAMIgXAKIghhOIAhBOIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAB2gSIBVgMgAB5g4IBHgqgABXB/gACxBAgAgUg8g");
    var mask_graphics_19 = new cjs.Graphics().p("AhiCTIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgRICNgeIiNAeQgDgRAAgUQAAgrAPgbIAAAAQAFgKAGgIQAMgQAQgLIAAAAQATgNAYgIIAMgDQAdgIAXgEIAIgBIAugHIAQBVIgNACIgLACIguhRIAuBRIgSAGIABAAIgIACIhVhKIBVBKQgKAFgHAFQgGAEgFAGIiAguICAAuIgEAIQgHAMAAARIABAMQACAIAEAHIAAAAQAFALALAHIgBAAIAMAFIANAEQANACAMAAIAEAAQAVAAAQgEQANgDAJgFQAMgHAIgIQAGgHAFgIQAGgLADgKIACgHIABgGQACgQAAgOIAAgCIAAglIAOgPIAjgkQAOAWAIAYQALAgAAAjIAAAJQgBAzgZAjIhRgnIBRAnQgPAUgXAPIABAAIADgBIgEABIgCADQgCADgZAMIgXAKIghhPIAhBPIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAB2gOIBVgNgAB5g0IBHgqgABXCDgACxBEgAAFhBg");
    var mask_graphics_20 = new cjs.Graphics().p("AhiCWIAAAAIgUgHIBdhaIhdBaQgTgIgPgMQgYgUgNgcIAAABQgHgOgDgQICNgeIiNAeQgDgRAAgVQAAgrAPgbIAAABQAFgKAGgJQAMgPAQgLIAAAAQATgOAYgHIAMgEQAdgHAXgFIAIgBIAugGIAMgBQAWgDATgDIAZBSIgNACIgxAIIgQhVIAQBVIgNACIgLACIguhSIAuBSIgSAFIABAAIgIADIhVhKIBVBKQgKAEgHAFQgGAFgFAGIiAgvICAAvIgEAIQgHAMAAAQIABANQACAIAEAGIAAABQAFAKALAHIgBAAIAMAFIANAEQANACAMABIAEAAQAVAAAQgEQANgDAJgFQAMgHAIgJQAGgHAFgHQAGgLADgKIACgHIABgHQACgQAAgNIAAgDIAAglIAOgOIAjgkQAOAWAIAXQALAgAAAkIAAAJQgBAzgZAiIhRgnIBRAnQgPAVgXAOIABAAIADAAIgEABIgCADQgCADgZAMIgXAKIghhPIAhBPIgdAKQgjALglACIAbhiIgbBiIgRAAQgkAAgfgIgAB2gKIBVgNgAB5gxIBHgqgABXCHgACxBHgAAdhBg");
    var mask_graphics_21 = new cjs.Graphics().p("AhiCYIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgQICNgeIiNAeQgDgSAAgUQAAgrAPgbIAAAAQAFgKAGgIQAMgPAQgMIAAAAQATgNAYgIIAMgDQAdgIAXgEIAIgBIAugHIAMgBQAWgCATgEIALgCIADgBIA3A9IgOAOIAAgBIgCABIgCABIgLAFIABAAIgQAEIgNADIgxAHIgNACIgLADIgSAFIABAAIgIADQgKAEgHAFQgGAFgFAFIiAguICAAuIgEAIQgHAMAAAQIABAOQACAHAEAHIAAAAQAFALALAHIgBAAIAMAFIANAEQANACAMAAIAEAAQAVAAAQgDQANgDAJgGQAMgHAIgIQAGgHAFgIQAGgLADgKIACgHIBVgNIhVANIABgGQACgQAAgOIAAgCIAAglIAOgOIAjgkQAOAVAIAYQALAgAAAjIAAAJQgBA0gZAiIhRgnIBRAnQgPAVgXAOIABAAIADgBIgEACIgCACQgCADgZAMIgXAKIghhOIAhBOIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAB5gvIBHgqgAgUgzIhVhLgAAFg7IguhSgAAdhAIgQhVgABbhKIgZhSgABXCIgACxBJg");
    var mask_graphics_22 = new cjs.Graphics().p("AhiCcIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgQICNgeIiNAeQgDgSAAgUQAAgrAPgbIAAAAQAFgKAGgIQAMgPAQgMIAAAAQATgNAYgIIAMgDQAdgIAXgEIAIgBIAugGIAMgCQAWgCATgEIALgCIADgBQALgDAJgFIBCAaIAEAHQAOAVAIAYQALAgAAAjIAAAJQgBA0gZAiIhRgnIBRAnQgPAVgXAOIABAAIADgBIgEACIgCACQgCADgZAMIgXAKIghhOIAhBOIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAgZA7IANAEQANACAMAAIAEAAQAVAAAQgDQANgDAJgGQAMgHAIgIQAGgHAFgIQAGgLADgLIACgGIBVgNIhVANIABgGQACgQAAgOIAAgCIAAglIAOgOIAjgkIgjAkIg3g9IA3A9IgOAOIAAgBIgCABIgCABIgLAFIABAAIgQAEIgNADIgxAHIgNACIgLADIgSAFIABAAIgIADQgKAEgHAFQgGAFgFAFIiAguICAAuIgEAIQgHAMAAAQIABAOQACAHAEAHIAAAAQAFALALAHIgBAAIAMAFgAB5grIBHgqgAgUgvIhVhLgAAFg3IguhSgAAdg8IgQhUgABbhGIgZhSgABXCMgACxBNg");
    var mask_graphics_23 = new cjs.Graphics().p("AhiCgIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgQICNgeIiNAeQgDgSAAgUQAAgrAPgbIAAAAQAFgJAGgJQAMgPAQgMIAAAAQATgNAYgIIAMgDQAdgIAXgEIAIgBIAugGIAMgCQAWgCATgEIALgCIADgBQALgDAJgFIALgGIAAgBIABgBIAeAFQANAOALAPIAEAHQAOAVAIAYQALAgAAAkIAAAJQgBAzgZAiIhRgnIBRAnQgPAVgXAOIABAAIADgBIgEACIgCADQgCACgZAMIgXAKIghhOIAhBOIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAgZA/IANAEQANACAMAAIAEAAQAVAAAQgDQANgDAJgGQAMgHAIgIQAGgHAFgIQAGgLADgLIACgGIBVgMIhVAMIABgGQACgQAAgOIAAgCIAAglIAOgOIAjgkIgjAkIg3g9IA3A9IgOAOIAAgBIgCABIgCABIgLAFIABAAIgQAEIgNADIgxAHIgNACIgLADIgSAFIABAAIgIADQgKAEgHAFQgGAFgFAFIiAguICAAuIgEAIQgHAMAAAQIABAOQACAHAEAHIAAAAQAFALALAHIgBAAIAMAFgAB5gnIBHgqgAgUgrIhVhLgAAFgzIguhSgAAdg4IgQhUgABbhCIgZhSgACmiFIhCgagABXCQgACxBRg");
    var mask_graphics_24 = new cjs.Graphics().p("AhiCoIAAAAIgUgHIBdhaIhdBaQgTgJgPgMQgYgTgNgcIAAAAQgHgNgDgRICNgeIiNAeQgDgRAAgUQAAgrAPgbIAAAAQAFgKAGgIQAMgQAQgLIAAAAQATgNAYgIIAMgDQAdgIAXgEIAIgBIAugHIAMgBQAWgCATgEIALgCIADgBQALgDAJgFIALgGIAAgBIABgBQAFgFACgHIABgEQAMAKAKAKQANAPALAPIAEAGQAOAWAIAYQALAgAAAjIAAAIQgBA0gZAjIhRgnIBRAnQgPAUgXAPIABAAIADgBIgEABIgCADQgCADgZAMIgXAKIghhPIAhBPIgdALQgjAKglACIAbhiIgbBiIgRABQgkAAgfgIgAgZBHIANAEQANACAMAAIAEAAQAVAAAQgEQANgDAJgFQAMgHAIgIQAGgHAFgIQAGgLADgLIACgHIBVgMIhVAMIABgGQACgPAAgOIAAgCIAAglIAOgPIAjgkIgjAkIg3g8IA3A8IgOAPIAAgBIgCABIgCABIgLAFIABAAIgQAEIgNADIgxAHIgNACIgLACIgSAGIABAAIgIACQgKAFgHAFQgGAEgFAGIiAguICAAuIgEAIQgHALAAARIABANQACAIAEAHIAAAAQAFALALAHIgBAAIAMAFgAB5gfIBHgqgAgUgkIhVhKgAAFgsIguhRgAAdgwIgQhVgABbg6IgZhSgACmh9IhCgagACOibIgegEgABXCYgACxBZg");

    this.timeline.addTween(cjs.Tween.get(mask).to({ graphics: null, x: 0, y: 0 }).wait(8).to({ graphics: mask_graphics_8, x: 53.3231, y: 13.3625 }).wait(1).to({ graphics: mask_graphics_9, x: 35.975, y: 27.1375 }).wait(1).to({ graphics: mask_graphics_10, x: 36.3875, y: 29.0625 }).wait(1).to({ graphics: mask_graphics_11, x: 35.2894, y: 33.2 }).wait(1).to({ graphics: mask_graphics_12, x: 33.2019, y: 36.3625 }).wait(1).to({ graphics: mask_graphics_13, x: 29.7769, y: 37.5125 }).wait(1).to({ graphics: mask_graphics_14, x: 24.5769, y: 37.5375 }).wait(1).to({ graphics: mask_graphics_15, x: 20.5144, y: 37.5375 }).wait(1).to({ graphics: mask_graphics_16, x: 20.3394, y: 37.5375 }).wait(1).to({ graphics: mask_graphics_17, x: 20.3394, y: 37.5375 }).wait(1).to({ graphics: mask_graphics_18, x: 20.3394, y: 37.2125 }).wait(1).to({ graphics: mask_graphics_19, x: 20.3394, y: 36.8375 }).wait(1).to({ graphics: mask_graphics_20, x: 20.3394, y: 36.475 }).wait(1).to({ graphics: mask_graphics_21, x: 20.3394, y: 36.325 }).wait(1).to({ graphics: mask_graphics_22, x: 20.3394, y: 35.925 }).wait(1).to({ graphics: mask_graphics_23, x: 20.3394, y: 35.5125 }).wait(1).to({ graphics: mask_graphics_24, x: 20.3394, y: 34.7375 }).wait(30));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhiCoIAAAAQgggKgWgSQgYgTgNgcIAAAAQgNgbAAgoQAAgrAPgbIAAAAQAOgbAZgSIAAAAQAYgQAfgIQAigJAagEIA6gIQAcgDAYgFQAVgGANgJIAAgBQAGgFACgIIABgEQBVBIgCBpQgBBTg/AnIABAAIADgBIgEABIgCADQgCADgZAMQgZAMgbAJQgrANguAAQgkAAgfgIgAgMBLQAPACAOAAQAmAAAVgMQAVgMAKgSQAGgLADgLIADgNQACgPAAgOIAAgoIgCABIgCABIgLAFIABAAQgPAFgOACIg+AJIgdAIIABAAQgPAFgKAHQgKAIgFAKQgHALAAARQAAAQAHAMIAAAAQAFALALAHIgBAAQAMAGANADg");
    this.shape.setTransform(20.3393, 34.7375);
    this.shape._off = true;

    var maskedShapeInstanceList = [this.shape];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(8).to({ _off: false }, 0).wait(46));

    // Layer_12 (mask)
    var mask_1 = new cjs.Shape();
    mask_1._off = true;
    var mask_1_graphics_0 = new cjs.Graphics().p("AgBAGQAAgBgBAAQAAAAAAAAQgBgBAAAAQgBAAAAgBIgBgDIABgBQACgEACAAQAAAAABAAQABAAAAAAQABABAAAAQABAAAAABIACACIAAABQAAADgDACQAAAAAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAg");
    var mask_1_graphics_1 = new cjs.Graphics().p("AhHAFIAAgFIAAgEICPAAIAAAEIgBAFg");
    var mask_1_graphics_2 = new cjs.Graphics().p("AhJAYIAAgFIABgEQABgUAFgSICMAYIgDAOIiPAAICPAAIgBAFIAAAEgABHAPg");
    var mask_1_graphics_3 = new cjs.Graphics().p("AhMAwIAAgFIAAgEQACgVAFgSQAGgUAMgRIAHgKIB5A2QgEAIgDAKIiLgZICLAZIgDAOIiPAAICPAAIAAAFIgBAEgABGAZg");
    var mask_1_graphics_4 = new cjs.Graphics().p("AhgBSIAAgFIAAgFQABgVAGgSQAGgVALgPIAIgLQATgZAbgQQAcgRAjgJIA0BdQgPAEgLAIQgJAGgFAKIh5g2IB5A2QgEAIgDAKIiLgZICLAZIgDAOIiPAAICPAAIAAAFIgBAFgAA5Aog");
    var mask_1_graphics_5 = new cjs.Graphics().p("AixBXIAAgFIAAgFQABgVAGgSQAGgVALgQIAIgKQATgZAbgQQAdgRAjgIIAKgDQAqgIAnAAQAjAAAoAGQAZADAWAIIhNBaQgJgEgKgCQgOgCgTAAQgSAAgPAEIgzhcIAzBcQgPAEgKAIQgJAGgFAKIh6g2IB6A2QgEAIgDAKIiMgZICMAZIgDAOIiQAAICQAAIAAAFIgBAFgAAQARg");
    var mask_1_graphics_6 = new cjs.Graphics().p("AjhBXIAAgFIAAgFQABgVAFgSQAHgVALgQIAHgKQAUgZAbgQQAdgRAjgIIAKgDQArgIAmAAQAjAAAoAGQAZADAWAIIhNBaIBNhaQAMAEAMAFQAgAQAUAcQAVAbgBAuIAAAQIiQADIAAgBIAAgBIAAgIQgBgPgFgJIAAAAQgGgLgLgHIgGgDQgJgEgKgCQgOgCgTAAQgRAAgPAEIg0hcIA0BcQgPAEgMAIQgIAGgFAKIh7g2IB7A2QgFAIgCAKIiNgZICNAZIgDAOIiQAAICQAAIAAAFIgBAFgAgfARgACChFIAAAAg");
    var mask_1_graphics_7 = new cjs.Graphics().p("ABSA8ICQgCIAAApIiPAEIgBgrgAjhBHIAAgFIAAgFQABgVAFgSQAHgUALgQIAHgLQAUgYAbgRQAdgRAjgIIAKgCQArgJAmAAQAjABAoAFQAZAEAWAHQAMAEAMAGQAgAPAUAcQAVAcgBAtIAAARIiQACIAAgBIAAAAIAAgIQgBgQgFgJIAAAAQgGgKgLgIIgGgDIBNhaIhNBaQgJgEgKgBQgOgCgTAAQgRAAgPADIg0hcIA0BcQgPAEgMAIQgIAHgFAKIh7g3IB7A3QgFAIgCAKIiNgaICNAaIgDANIiQAAICQAAIAAAGIgBAEgADiA6gAgfABg");
    var mask_1_graphics_8 = new cjs.Graphics().p("ABTBKICPgEIiPAEIgBgrICQgCIAAApIAAA6IiPADIAAg5gAjhAqIAAgFIAAgEQABgVAFgSQAHgUALgRIAHgKQAUgZAbgQQAdgRAjgJIAKgCQArgIAmAAQAjAAAoAFQAZAEAWAHQAMAEAMAGQAgAPAUAcQAVAcgBAuIAAAQIiQACIAAgBIAAAAIAAgIQgBgPgFgIIAAAAQgGgLgLgHIgGgEIBNhbIhNBbQgJgEgKgBQgOgCgTAAQgRAAgPAEIg0heIA0BeQgPADgMAIQgIAHgFAJIh7g2IB7A2QgFAIgCAKIiNgZICNAZIgDAOIiQAAICQAAIAAAFIgBAEgADiAdgAgfgag");
    var mask_1_graphics_9 = new cjs.Graphics().p("ABRCrIAAg9IABArIABhDIAAg5ICPgEIiPAEIgBgqICQgCIAAAoIAAA6IiPADICPgDIAABaIiSADIABgFgADiBTgAjhgCIAAgFIAAgEQABgWAFgSQAHgUALgRIAHgLQAUgYAbgQQAdgRAjgJIAKgCQArgIAmAAQAjAAAoAFQAZAEAWAHQAMAEAMAGQAgAPAUAcQAVAcgBAvIAAAQIiQACIAAgBIAAAAIAAgIQgBgQgFgIIAAAAQgGgLgLgHIgGgEIBNhbIhNBbQgJgEgKgBQgOgCgTAAQgRAAgPADIg0hdIA0BdQgPAEgMAIQgIAHgFAKIh7g4IB7A4QgFAIgCAKIiNgaICNAaIgDAOIiQAAICQAAIAAAFIgBAEgADigPgAgfhIg");
    var mask_1_graphics_10 = new cjs.Graphics().p("ABOCYIACgIIAAgGIABg9IABAsIABhDIAAg5ICPgDIiPADIgBgqICQgDIAAAqIAAA4IiPAEICPgEIAABaIiSAEICSgEIAAAxIABAQIiUAEQgDgcACgdgADiCMgADiAygAjigiIAAgFIABgFQABgVAFgSQAGgVAMgQIAHgLQAUgZAbgQQAdgRAjgJIAKgCQArgIAlAAQAkAAAoAGQAZADAWAIQAMAEALAFQAgAQAVAcQAUAcAAAuIAAAQIiQADIAAgBIAAgBIAAgIQgBgPgFgJIAAAAQgGgLgLgHIgGgDIBNhbIhNBbQgJgEgKgCQgOgCgTAAQgSAAgOAEIg0heIA0BeQgPAEgMAIQgIAGgGAKIh6g3IB6A3QgEAIgCAKIiNgZICNAZIgDAOIiQAAICQAAIgBAFIAAAFgADigwgAgfhog");
    var mask_1_graphics_11 = new cjs.Graphics().p("ABPC9ICTgEIiTAEQgDgcABgeIACgHIABgGIABg9IABAsIAAhEIAAg4ICQgEIAAA5IiQADICQgDIAABaIiTAEICTgEIAAAwIAAARIABAKIiOAiQgEgUgCgUgADiB4gADiAegABSgXIAAgrICQgCIAAApIiQAEIAAAAgADigbgAjig3IAAgFIAAgEQACgVAFgSQAGgVAMgRIAHgKQATgZAcgQQAcgRAjgJIALgCQArgIAlAAQAkAAAoAFQAZAEAWAHQAMAFALAFQAgAPAVAcQAUAcAAAvIAAAQIiQACIAAgBIAAAAIAAgIQgBgPgFgJIAAAAQgGgLgLgHIgGgDIBNhcIhNBcQgKgFgJgBQgOgCgTAAQgSAAgPAEIg0heIA0BeQgPAEgLAHQgIAHgGAKIh6g3IB6A3QgEAIgDAKIiMgZICMAZIgCAOIiRAAICRAAIgBAFIAAAEgADihEgAggh8g");
    var mask_1_graphics_12 = new cjs.Graphics().p("ABcDyIgBgFIgEgOIgEgMIADAJIgCgJICOgiIiOAiQgEgUgCgUICUgEIiUAEQgCgdABgdIACgIIABgFIAAg9IACAsIAAhEIAAg4ICQgEIAAA5IiQADICQgDIAABaIiTADICTgDIAAAwIAAARIAAAKIABALIiFA7gADiBmgADiAMgABSgpIAAgrICQgCIiQACIAAgBIAAAAIgBgIQAAgPgFgJIgBAAQgGgLgKgHIgHgEQgJgEgKgBQgOgCgTAAQgRAAgPAEIg0heIA0BeQgPADgLAIQgJAHgFAKIh6g3IB6A3QgEAIgDAKIiMgaICMAaIgDAOIiQAAICQAAIAAAFIgBAEIiPAAIAAgFIAAgEQABgVAGgTQAGgUALgRIAIgKQATgZAbgQQAdgRAjgJIAKgCQArgIAmAAQAjAAAoAFQAZAEAWAHIhNBbIBNhbQAMAEAMAGQAgAPAUAcQAVAcAAAvIAAAQIAAApIiQAEIAAAAgADigtgAggiOgACBjmIAAAAg");
    var mask_1_graphics_13 = new cjs.Graphics().p("ABhD+IgFgPICGg8IiGA8IgCgGIgBgEIgDgOIgEgMIADAJIgCgJICOgiIiOAiQgEgUgCgVICUgEIiUAEQgCgcABgdIACgIIABgFIAAg9IABArIABhDIAAg4ICPgEIAAA5IiPADICPgDIAABaIiSADICSgDIAAAwIABAQIAAALIABAKIACAZIiCA0gADgBegADgAEgABRgxIgBgrICQgCIiQACIAAgBIAAAAIAAgIQgBgQgFgJIAAAAQgGgKgLgIIgGgDQgJgEgKgCQgOgCgSAAQgSAAgPAEIg0hdIA0BdQgPAEgMAIQgIAHgFAKIh7g4IB7A4QgFAIgCAKIiNgaICNAaIgDANIiQAAICQAAIAAAGIgBAEIiPAAIAAgFIAAgFQABgVAFgSQAHgUALgRIAHgLQAUgYAbgRQAdgRAjgIIAKgCQArgJAmAAQAjABAoAFQAZAEAWAHIhNBbIBNhbQAMAEAMAGQAgAPAUAcQAVAcgBAuIAAARIAAApIiPAEIAAAAgADgg1gAghiXgACAjuIAAAAg");
    var mask_1_graphics_14 = new cjs.Graphics().p("ABgEAICDg0IAAAAIADATIhTAhgABgEAIAAgCIgFgPICFg8IiFA8IgCgGIgBgEIgEgOIgEgMIADAJIgCgJICPgiIiPAiQgEgUgCgVICUgEIiUAEQgCgcABgdIACgIIABgFIABg9IABArIAAhDIAAg4ICQgEIAAA5IiQADICQgDIAABaIiTADICTgDIAAAwIAAAQIABALIAAAKIADAZIiDA0gADjDMgADfBegADfAEgABPgxIAAgrICQgCIiQACIAAgBIAAAAIAAgIQgBgQgFgJIgBAAQgGgKgKgIIgHgDQgJgEgKgCQgOgCgSAAQgSAAgPAEIg0hdIA0BdQgPAEgLAIQgJAHgFAKIh6g4IB6A4QgEAIgDAKIiMgaICMAaIgDANIiQAAICQAAIAAAGIgBAEIiPAAIAAgFIAAgFQABgVAGgSQAGgUALgRIAIgLQATgYAbgRQAdgRAjgIIAKgCQArgJAmAAQAjABAoAFQAZAEAWAHIhNBbIBNhbQANAEALAGQAgAPAUAcQAVAcAAAuIAAARIAAApIiQAEIAAAAgADfg1gAgjiXgAB+juIAAAAg");
    var mask_1_graphics_15 = new cjs.Graphics().p("ACPEAIBUghIhUAhIgzAAICDg0IAAAAIAEATQACAMAFAJIgeAMgABcEAIAAgCIgFgPICGg8IiGA8IgCgGIgBgEIgDgOIgEgMIADAJIgCgJICOgiIiOAiQgEgUgCgVICUgEIiUAEQgDgcACgdIACgIIAAgFIABg9IABArIABhDIAAg4ICPgEIAAA5IiPADICPgDIAABaIiSADICSgDIAAAwIABAQIAAALIABAKIACAZIiDA0gADfDMgADbBegADbAEgABMgxIgBgrICQgCIiQACIAAgBIAAAAIAAgIQgBgQgFgJIAAAAQgGgKgLgIIgGgDQgJgEgKgCQgOgCgSAAQgTAAgOAEIg0hdIA0BdQgPAEgMAIQgIAHgGAKIh6g4IB6A4QgEAIgCAKIiNgaICNAaIgDANIiQAAICQAAIgBAGIAAAEIiQAAIAAgFIABgFQABgVAFgSQAGgUAMgRIAHgLQAUgYAbgRQAdgRAjgIIAKgCQArgJAlAAQAkABAoAFQAZAEAWAHIhNBbIBNhbQAMAEALAGQAgAPAVAcQAUAcAAAuIAAARIAAApIiPAEIAAAAgADbg1gAgmiXgAB7juIAAAAg");
    var mask_1_graphics_16 = new cjs.Graphics().p("ADJEAIAdgMIgdAMIg9AAIBTghIhTAhIgzAAICDg0IAAAAIADATQADAMAEAJIACAEIAFAIgABZEAIAAgCIgFgPICGg8IiGA8IgCgGIgBgEIgEgOIgEgMIADAJIgBgJICOgiIiOAiQgEgUgDgVICUgEIiUAEQgCgcABgdIACgIIABgFIABg9IABArIAAhDIAAg4ICQgEIAAA5IiQADICQgDIAABaIiTADICTgDIAAAwIAAAQIABALIABAKIACAZIiDA0gADcDMgADYBegADYAEgABIgxIAAgrICQgCIiQACIAAgBIAAAAIAAgIQgBgQgFgJIAAAAQgHgKgKgIIgHgDQgJgEgKgCQgNgCgTAAQgSAAgPAEIg0hdIA0BdQgPAEgLAIQgIAHgGAKIh6g4IB6A4QgEAIgDAKIiMgaICMAaIgDANIiQAAICQAAIAAAGIgBAEIiPAAIAAgFIAAgFQACgVAFgSQAGgUAMgRIAHgLQATgYAcgRQAcgRAjgIIAKgCQArgJAnAAQAiABApAFQAZAEAWAHIhOBbIBOhbQAMAEALAGQAgAPAUAcQAVAcAAAuIAAARIAAApIiQAEIAAAAgADYg1gAgqiXgAB4juIAAAAg");

    this.timeline.addTween(cjs.Tween.get(mask_1).to({ graphics: mask_1_graphics_0, x: 2.575, y: 21.2 }).wait(1).to({ graphics: mask_1_graphics_1, x: 8.725, y: 16.875 }).wait(1).to({ graphics: mask_1_graphics_2, x: 8.8625, y: 14.9125 }).wait(1).to({ graphics: mask_1_graphics_3, x: 9.2, y: 12.5125 }).wait(1).to({ graphics: mask_1_graphics_4, x: 11.2125, y: 9.1875 }).wait(1).to({ graphics: mask_1_graphics_5, x: 19.325, y: 8.675 }).wait(1).to({ graphics: mask_1_graphics_6, x: 24.15, y: 8.675 }).wait(1).to({ graphics: mask_1_graphics_7, x: 24.15, y: 10.25 }).wait(1).to({ graphics: mask_1_graphics_8, x: 24.15, y: 13.125 }).wait(1).to({ graphics: mask_1_graphics_9, x: 24.15, y: 17.6375 }).wait(1).to({ graphics: mask_1_graphics_10, x: 24.1625, y: 20.8875 }).wait(1).to({ graphics: mask_1_graphics_11, x: 24.1875, y: 22.9125 }).wait(1).to({ graphics: mask_1_graphics_12, x: 24.225, y: 24.725 }).wait(1).to({ graphics: mask_1_graphics_13, x: 24.35, y: 25.55 }).wait(1).to({ graphics: mask_1_graphics_14, x: 24.5125, y: 25.55 }).wait(1).to({ graphics: mask_1_graphics_15, x: 24.8625, y: 25.55 }).wait(1).to({ graphics: mask_1_graphics_16, x: 25.2, y: 25.55 }).wait(38));

    // Layer_8
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AiLD9IAAAAQghgKgVgRQgZgUgNgbIAAAAQgNgcAAgoQAAgrAQgbIAAAAQAOgcAZgQIAAAAQAYgRAegHQAjgJAagFIA7gHQAagDAYgFQAWgGANgKIAAAAQALgKgCgRIAAAAQABgWgHgLIAAAAQgGgKgLgIQgMgHgNgCQgOgCgSAAQglAAgXAQQgUAQgDAmIgBAEIiPAAIAAgFQABgwAXghQAVggAhgUQAhgTApgIQArgJAmAAQAjABAoAFQAnAGAgAPQAgAPAUAcQAVAcgBAuIAAD9QABAfADAfQAEAdAJAPIAEAIIiTAAIgBgEIgIgXIgBgPQgeAcgoANQgqANgvgBQglABgegJgAg1ChQAQACAOAAQAlAAAVgMQAVgNAJgRQALgSACgRQABgRAAgNIAAgoQgGAEgIADIAAAAQgPAEgNACIg+AKIgdAHIAAAAQgOAGgKAHQgLAHgEALQgHAMAAARQAAAQAHAMIAAAAQAFAKAKAIIAAAAQAMAGANADg");
    this.shape_1.setTransform(24.45, 26.15);

    var maskedShapeInstanceList = [this.shape_1];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(54));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-7.7, -11.4, 65.2, 75.5);


  (lib.A = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_2 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_17 = new cjs.Graphics().p("AgEAIQgBgBAAAAQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAgGAIgCQAFgBADAHQACADgDAEQgCAEgFAAIAAAAIgEgBg");
    var mask_graphics_18 = new cjs.Graphics().p("AAAgHIAEABIAAANIgHABIADgPg");
    var mask_graphics_19 = new cjs.Graphics().p("AACgKIAEgQIAEABIAAAOIgIABIAIgBIAAAmIgTAAIALglgAAKgLg");
    var mask_graphics_20 = new cjs.Graphics().p("AgIAaIAIgXIAMgmIAEgPIAEABIAAAOIgIAAIAIAAIAAAlIgUABIAUgBIAAAwIgnABIALgZgAAUACgAAUgjg");
    var mask_graphics_21 = new cjs.Graphics().p("AgBAVIAKgYIAJgYIAMgmIAHgBIgHABIAEgQIADABIAAAOIAAAnIgTAAIATAAIAAAvIgmABIAmgBIAAA8IhJACQATgcAQghgAAlAUgAAlgbg");
    var mask_graphics_22 = new cjs.Graphics().p("AgZBFIADgFQATgbAPgiIALgYIAJgXIAMgnIAHAAIgHAAIAEgPIADABIAAAOIAAAmIgTABIATgBIAAAvIgnABIAngBIAAA8IhJACIBJgCIAAAjIhlACQANgOAMgQgAAzA+gAAzACgAAzgtg");
    var mask_graphics_23 = new cjs.Graphics().p("AgnBZQANgPALgQIAEgEQASgcAQggIALgZIAJgYIALgmIAIgBIgIABIAFgQIADABIAAAOIAAAnIgTAAIATAAIAAAwIgnABIAngBIAAA7IhJACIBJgCIAAAiIhlADIBlgDIAAAXIh7ABIAWgVgAA+BWgAA+A0gAA+gHgAA+g3g");
    var mask_graphics_24 = new cjs.Graphics().p("AgvBjIAWgWQANgOALgQIADgFQATgbAQghIALgZIAJgXIALgmIAIgBIgIABIAFgQIADABIAAAOIAAAnIgTAAIATAAIAAAvIgnABIAngBIAAA7IhKACIBKgCIAAAjIhlACIBlgCIAAAWIh7ACIB7gCIAAAWIiXACQAPgKANgMgABMBhgABMBLgABMAogABMgTgABMhCg");
    var mask_graphics_25 = new cjs.Graphics().p("AhNB0IAEgDQAPgLANgMIAWgVQANgOAKgRIAEgEQATgcAQggIALgZIAJgXIALgnIAIgBIgIABIAFgQIADABIAAAOIAAAnIgTABIATgBIAAAwIgnABIAngBIAAA7IhKACIBKgCIAAAiIhlADIBlgDIAAAXIh7ABIB7gBIAAAWIiXACICXgCIAAARIibACgABOBvgABOBZgABOBCgABOAggABOgbgABOhLg");
    var mask_graphics_26 = new cjs.Graphics().p("AhJCGIgEgFIAAgDIAAgPIAEgCQAPgLANgMIAWgVQANgOAKgRIAEgEQATgcAQggIALgZIAJgYIALgmIAIgBIgIABIAFgQIADABIAAAOIAAAnIgTAAIATAAIAAAwIgnABIAngBIAAA7IhKACIBKgCIAAAiIhlADIBlgDIAAAXIh7ABIB7gBIAAAWIiXACICXgCIAAARIibACICbgCIAAAKgABOB8gABOBrgABOBVgABOA+gABOAcgABOgfgABOhPg");

    this.timeline.addTween(cjs.Tween.get(mask).to({ graphics: null, x: 0, y: 0 }).wait(17).to({ graphics: mask_graphics_17, x: 59.1568, y: 24.4983 }).wait(1).to({ graphics: mask_graphics_18, x: 58.625, y: 26.0125 }).wait(1).to({ graphics: mask_graphics_19, x: 58.025, y: 27.9375 }).wait(1).to({ graphics: mask_graphics_20, x: 57.0375, y: 30.3625 }).wait(1).to({ graphics: mask_graphics_21, x: 55.275, y: 33.4125 }).wait(1).to({ graphics: mask_graphics_22, x: 53.875, y: 35.1625 }).wait(1).to({ graphics: mask_graphics_23, x: 52.8, y: 36.225 }).wait(1).to({ graphics: mask_graphics_24, x: 51.4, y: 37.35 }).wait(1).to({ graphics: mask_graphics_25, x: 51.2, y: 38.2 }).wait(1).to({ graphics: mask_graphics_26, x: 51.2, y: 38.6125 }).wait(23));

    // Layer_7
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhNCMIAAgWQBwhOArizIAAEXg");
    this.shape.setTransform(51.2, 37.975);
    this.shape._off = true;

    var maskedShapeInstanceList = [this.shape];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(17).to({ _off: false }, 0).wait(32));

    // Layer_5 (mask)
    var mask_1 = new cjs.Shape();
    mask_1._off = true;
    var mask_1_graphics_0 = new cjs.Graphics().p("AhJABIAAgBICTAAIAAABg");
    var mask_1_graphics_1 = new cjs.Graphics().p("AhJADIAAgHIAAgCICTAAIAAACIiTAAICTAAIAAAKIAAABgABKgEg");
    var mask_1_graphics_2 = new cjs.Graphics().p("AhJAIIAAgSIAAgCICTAAIAAACIiTAAICTAAIAAAKIAAAOgABKgKg");
    var mask_1_graphics_3 = new cjs.Graphics().p("AhKAQIABgmIAAgCICUAAIAAACIiUAAICUAAIAAAKIgCAlgABLgWg");
    var mask_1_graphics_4 = new cjs.Graphics().p("AhLAfIADgmIABgnIAAgCICTAAIAAACIiTAAICTAAIAAALIgBAkIiTgIICTAIIgFAwgABMgug");
    var mask_1_graphics_5 = new cjs.Graphics().p("AhPAxQAEgXADgYIADgmIABgmIAAgCICUAAIAAACIiUAAICUAAIAAAKIgCAlIiTgJICTAJIgEAvIiSgSICSASQgEAegFAcgABQhKg");
    var mask_1_graphics_6 = new cjs.Graphics().p("AhVBGIABgFQAHgYAEgZQAFgXACgXIADgnIABgnIAAgCICUAAIAAACIiUAAICUAAIAAALIgCAlIiTgJICTAJIgEAwIiSgSICSASQgDAcgFAcIiRgcICRAcQgHAjgIAggABWhsg");
    var mask_1_graphics_7 = new cjs.Graphics().p("AhfBdQAMgbAJgiIABgEQAGgYAFgZQAEgXADgXIADgnIABgnIAAgCICTAAIAAACIiTAAICTAAIAAAKIgBAmIiTgJICTAJIgFAwIiRgSICRASQgDAdgFAbIiQgcICQAcQgGAjgJAgIiNgqICNAqQgLAngPAjgABgiRg");
    var mask_1_graphics_8 = new cjs.Graphics().p("Ah1B1QAZgbASgpQAMgbAJggIACgFQAGgYAFgZQAEgXACgYIAEgnIABgnIAAgCICTAAIAAACIiTAAICTAAIAAALIgCAlIiSgJICSAJIgEAwIiSgSICSASQgDAegFAbIiQgcICQAcQgGAjgJAfIiOgoICOAoQgLAngPAjIiJg3ICJA3QgRApgWAiIgIANgAB2i9g");
    var mask_1_graphics_9 = new cjs.Graphics().p("AiHBqIAKgHQAOgJAMgNQAZgaASgqQAMgaAJghIABgFQAHgYAEgZQAFgXACgYIADgnIABgmIAAgCICUAAIAAACIiUAAICUAAIAAAKIgCAlIiTgJICTAJIgEAwIiSgSICSASQgDAegFAcIiRgdICRAdQgHAigIAgIiOgpICOApQgLAmgPAjIiJg3ICJA3QgRApgXAjIgIAMIiEhLICEBLQghAxgpAegACIjkg");
    var mask_1_graphics_10 = new cjs.Graphics().p("AigBhIATgCQAQgEAPgIIALgHQANgKAMgMQAZgbASgoQAMgbAJghIACgFQAGgYAEgZQAEgXACgYIAEgnIABgnIAAgCICUAAIAAACIiUAAICUAAIAAALIgCAlIiTgJICTAJIgEAwIiTgSICTASQgDAdgFAcIiRgcICRAcQgGAjgJAgIiOgpICOApQgLAmgPAjIiJg2ICJA2QgRApgWAiIgIANIiFhLICFBLQgiAxgpAeIheh9IBeB9Qg3AphGAGgAChj8g");
    var mask_1_graphics_11 = new cjs.Graphics().p("AjODrIgOgGIBDiQQAWAMAbAAIADAAIATgDQARgDAPgJIAKgHQAOgJAMgNQAYgaASgoQAMgcAJghIABgEQAHgZAEgZQAEgXADgYIADgnIABgmIAAgCICVAAIAAACIiVAAICVAAIAAAKIgCAmIiUgKICUAKIgEAwIiTgTICTATQgEAdgFAcIiRgdICRAdQgGAjgIAfIiPgpICPApQgLAngPAjIiKg2ICKA2QgRAogXAjIgIAMIiEhLICEBLQgiAxgpAfIhdh+IBdB+Qg3AohFAGIgUidIAUCdIgXABQg2AAgwgUgADdj8g");
    var mask_1_graphics_12 = new cjs.Graphics().p("AiXDrIgOgGIBDiQIhDCQQgogUggghQgWgXgQgaICIhSQAGALAJAJQAMAMAOAIQAWAMAbAAIAEAAIASgDQARgDAOgJIAKgHQAOgJAMgNQAZgaASgoQAMgcAJghIABgEQAHgZAEgZQAFgXACgYIADgnIABgmIAAgCICVAAIAAACIiVAAICVAAIAAAKIgCAmIiUgKICUAKIgEAwIiTgTICTATQgDAdgFAcIiSgdICSAdQgHAjgIAfIiPgpICPApQgLAngPAjIiKg2ICKA2QgRAogXAjIgIAMIiFhLICFBLQgiAxgpAfIheh+IBeB+Qg4AohEAGIgTidIATCdIgXABQg2AAgwgUgAEUj8g");
    var mask_1_graphics_13 = new cjs.Graphics().p("AiEDrIgOgGIBDiQIhDCQQgogUggghQgWgXgQgaICIhSIiIBSQgKgSgIgTQgUgxAAg0QAAgPACgPICdAVIgBAJQAAAfAQAZQAGALAKAJQALAMAOAIQAWAMAbAAIAEAAIASgDQAQgDAPgJIAKgHQAOgJAMgNQAZgaASgoQAMgcAJghIABgEQAHgZAEgZQAFgXACgYIADgnIACgmIAAgCICUAAIAAACIiUAAICUAAIAAAKIgCAmIiUgKICUAKIgEAwIiTgTICTATQgDAdgFAcIiSgdICSAdQgGAjgJAfIiPgpICPApQgLAngPAjIiKg2ICKA2QgRAogWAjIgIAMIiGhLICGBLQgiAxgqAfIheh+IBeB+Qg4AohEAGIgTidIATCdIgXABQg1AAgxgUgAEnj8g");
    var mask_1_graphics_14 = new cjs.Graphics().p("AiEDrIgOgGIBDiQIhDCQQgogUggghQgWgXgQgaICIhSIiIBSQgKgSgIgTQgUgxAAg0QAAgPACgPQAEglANgiQANghAVgcIB8BjQgQAXgCAfIidgVICdAVIgBAJQAAAfAQAZQAGALAKAJQALAMAOAIQAWAMAbAAIAEAAIASgDQAQgDAPgJIAKgHQAOgJAMgNQAZgaASgoQAMgcAJghIABgEQAHgZAEgZQAFgXACgYIADgnIACgmIAAgCICUAAIAAACIiUAAICUAAIAAAKIgCAmIiUgKICUAKIgEAwIiTgTICTATQgDAdgFAcIiSgdICSAdQgGAjgJAfIiPgpICPApQgLAngPAjIiKg2ICKA2QgRAogWAjIgIAMIiGhLICGBLQgiAxgqAfIheh+IBeB+Qg4AohEAGIgTidIATCdIgXABQg1AAgxgUgAiHgUgAEnj8g");
    var mask_1_graphics_15 = new cjs.Graphics().p("AiEDzIgOgGIBDiQIhDCQQgogUggghQgWgXgQgaICIhSIiIBSQgKgSgIgTQgUgxAAg0QAAgPACgPQAEglANghQANgiAVgcQAJgMAMgMQAkgnAxgUQAVgKAXgFIAmCaQgeAGgXAYIgLANIh8hjIB8BjQgQAXgCAfIidgVICdAVIgBAJQAAAfAQAZQAGALAKAKQALAMAOAHQAWAMAbAAIAEAAIASgCQAQgEAPgJIAKgGQAOgKAMgMQAZgbASgoQAMgbAJgiIABgEQAHgYAEgaQAFgXACgXIADgnIACgnIAAgCICUAAIAAACIiUAAICUAAIAAAKIgCAmIiUgJICUAJIgEAwIiTgSICTASQgDAdgFAcIiSgdICSAdQgGAjgJAgIiPgqICPAqQgLAmgPAjIiKg2ICKA2QgRAogWAjIgIAMIiGhKICGBKQgiAygqAeIheh+IBeB+Qg4AphEAGIgTieIATCeIgXABQg1AAgxgVgAh1hCgAEnj0g");
    var mask_1_graphics_16 = new cjs.Graphics().p("AiED2IgOgGIBDiQIhDCQQgogUggghQgWgWgQgbICIhRIiIBRQgKgRgIgUQgUgwAAg1QAAgPACgPQAEgkANgiQANgiAVgcQAJgMAMgMQAkgmAxgVQAVgJAXgFQAcgHAfAAQAQAAAQACIgNCeIgTgBQgLAAgKACIgmiaIAmCaQgeAFgXAYIgLANIh8hjIB8BjQgQAYgCAeIidgVICdAVIgBAJQAAAgAQAZQAGAKAKAKQALAMAOAHQAWAMAbAAIAEAAIASgCQAQgEAPgIIAKgHQAOgKAMgMQAZgbASgoQAMgbAJghIABgFQAHgYAEgZQAFgXACgYIADgnIACgnIAAgCICUAAIAAACIiUAAICUAAIAAALIgCAlIiUgJICUAJIgEAwIiTgSICTASQgDAdgFAcIiSgcICSAcQgGAjgJAgIiPgpICPApQgLAmgPAjIiKg2ICKA2QgRApgWAiIgIANIiGhLICGBLQgiAxgqAeIheh9IBeB9Qg4AphEAGIgTieIATCeIgXABQg1AAgxgVgAEnjxg");
    var mask_1_graphics_17 = new cjs.Graphics().p("AiED2IgOgGIBDiQIhDCQQgogUggghQgWgWgQgbICIhRIiIBRQgKgRgIgUQgUgwAAg1QAAgPACgPQAEgkANgiQANgiAVgcQAJgMAMgMQAkgmAxgVQAVgJAXgFQAcgHAfAAQAQAAAQACQAZAEAaAIIAIADIASAIIgcCiQgMgJgNgGIAbieIgbCeQgPgHgQgEIgGgBIANieIgNCeIgTgBQgLAAgKACIgmiaIAmCaQgeAFgXAYIgLANIh8hjIB8BjQgQAYgCAeIidgVICdAVIgBAJQAAAgAQAZQAGAKAKAKQALAMAOAHQAWAMAbAAIAEAAIASgCQAQgEAPgIIAKgHQAOgKAMgMQAZgbASgoQAMgbAJghIABgFQAHgYAEgZQAFgXACgYIADgnIACgnIAAgCICUAAIAAACIiUAAICUAAIAAALIgCAlIiUgJICUAJIgEAwIiTgSICTASQgDAdgFAcIiSgcICSAcQgGAjgJAgIiPgpICPApQgLAmgPAjIiKg2ICKA2QgRApgWAiIgIANIiGhLICGBLQgiAxgqAeIheh9IBeB9Qg4AphEAGIgTieIATCeIgXABQg1AAgxgVgAEnjxg");
    var mask_1_graphics_18 = new cjs.Graphics().p("AiED2IgOgGIBDiQIhDCQQgogUggghQgWgWgQgbICIhRIiIBRQgKgRgIgUQgUgwAAg1QAAgPACgPQAEgkANgiQANgiAVgcQAJgMAMgMQAkgmAxgVQAVgJAXgFQAcgHAfAAQAQAAAQACQAZAEAaAIIAIADIASAIIASAKIgdCoQgIgJgJgHIAciiIgcCiQgMgJgNgGIAbieIgbCeQgPgHgQgEIgGgBIANieIgNCeIgTgBQgLAAgKACIgmiaIAmCaQgeAFgXAYIgLANIh8hjIB8BjQgQAYgCAeIidgVICdAVIgBAJQAAAgAQAZQAGAKAKAKQALAMAOAHQAWAMAbAAIAEAAIASgCQAQgEAPgIIAKgHQAOgKAMgMQAZgbASgoQAMgbAJghIABgFQAHgYAEgZQAFgXACgYIADgnIACgnIAAgCICUAAIAAACIiUAAICUAAIAAALIgCAlIiUgJICUAJIgEAwIiTgSICTASQgDAdgFAcIiSgcICSAcQgGAjgJAgIiPgpICPApQgLAmgPAjIiKg2ICKA2QgRApgWAiIgIANIiGhLICGBLQgiAxgqAeIheh9IBeB9Qg4AphEAGIgTieIATCeIgXABQg1AAgxgVgAEnjxg");
    var mask_1_graphics_19 = new cjs.Graphics().p("AiED2IgOgGIBDiQIhDCQQgogUggghQgWgWgQgbICIhRIiIBRQgKgRgIgUQgUgwAAg1QAAgPACgPQAEgkANgiQANgiAVgcQAJgMAMgMQAkgmAxgVQAVgJAXgFQAcgHAfAAQAQAAAQACQAZAEAaAIIAIADIASAIIASAKQAJAGAGAGIAGAHQAAAwgKAyQgJAugMAdIgFgHIAeitIgeCtIgOgRIAdioIgdCoQgIgJgJgHIAciiIgcCiQgMgJgNgGIAbieIgbCeQgPgHgQgEIgGgBIANieIgNCeIgTgBQgLAAgKACIgmiaIAmCaQgeAFgXAYIgLANIh8hjIB8BjQgQAYgCAeIidgVICdAVIgBAJQAAAgAQAZQAGAKAKAKQALAMAOAHQAWAMAbAAIAEAAIASgCQAQgEAPgIIAKgHQAOgKAMgMQAZgbASgoQAMgbAJghIABgFQAHgYAEgZQAFgXACgYIADgnIACgnIAAgCICUAAIAAACIiUAAICUAAIAAALIgCAlIiUgJICUAJIgEAwIiTgSICTASQgDAdgFAcIiSgcICSAcQgGAjgJAgIiPgpICPApQgLAmgPAjIiKg2ICKA2QgRApgWAiIgIANIiGhLICGBLQgiAxgqAeIheh9IBeB9Qg4AphEAGIgTieIATCeIgXABQg1AAgxgVgAEnjxg");

    this.timeline.addTween(cjs.Tween.get(mask_1).to({ graphics: mask_1_graphics_0, x: 51.575, y: 2.4 }).wait(1).to({ graphics: mask_1_graphics_1, x: 51.5748, y: 2.975 }).wait(1).to({ graphics: mask_1_graphics_2, x: 51.5625, y: 3.65 }).wait(1).to({ graphics: mask_1_graphics_3, x: 51.525, y: 4.8 }).wait(1).to({ graphics: mask_1_graphics_4, x: 51.3625, y: 7.1875 }).wait(1).to({ graphics: mask_1_graphics_5, x: 51.025, y: 10.05 }).wait(1).to({ graphics: mask_1_graphics_6, x: 50.4125, y: 13.3875 }).wait(1).to({ graphics: mask_1_graphics_7, x: 49.3625, y: 17.1 }).wait(1).to({ graphics: mask_1_graphics_8, x: 47.1875, y: 21.4625 }).wait(1).to({ graphics: mask_1_graphics_9, x: 45.4125, y: 25.45 }).wait(1).to({ graphics: mask_1_graphics_10, x: 42.8875, y: 27.775 }).wait(1).to({ graphics: mask_1_graphics_11, x: 36.925, y: 27.825 }).wait(1).to({ graphics: mask_1_graphics_12, x: 31.4125, y: 27.825 }).wait(1).to({ graphics: mask_1_graphics_13, x: 29.5, y: 27.825 }).wait(1).to({ graphics: mask_1_graphics_14, x: 29.5, y: 27.825 }).wait(1).to({ graphics: mask_1_graphics_15, x: 29.5, y: 27 }).wait(1).to({ graphics: mask_1_graphics_16, x: 29.5, y: 26.675 }).wait(1).to({ graphics: mask_1_graphics_17, x: 29.5, y: 26.675 }).wait(1).to({ graphics: mask_1_graphics_18, x: 29.5, y: 26.675 }).wait(1).to({ graphics: mask_1_graphics_19, x: 29.5, y: 26.675 }).wait(30));

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AiED2QgxgUglgnQgkgmgUgwQgUgwAAg1QAAg0ATgwQATgxAkglQAkgmAxgVQAwgVA3AAQAtAAAuARQArARAOAUQAAAwgKAyQgJAugMAdQgkg2g4gMQgOgCgLAAQgsAAgeAfQgdAfgBAtQAAAtAgAgQAeAfAsAAQAKAAAMgCQAWgFATgOQA+grAdhvQAXhTAAhdICUAAIAAANQgHDehXCHQhZCMiOAAQg1AAgxgVg");
    this.shape_1.setTransform(29.5, 26.675);

    var maskedShapeInstanceList = [this.shape_1];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
    }

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(49));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-6, -4.7, 70.8, 64.7);


  (lib.Symbol = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhHA8IAAh3ICPAAIAAB3g");
    this.shape.setTransform(7.2, 6.025);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

  }).prototype = getMCSymbolPrototype(lib.Symbol, new cjs.Rectangle(0, 0, 14.4, 12.1), null);


  (lib.i = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // _
    this.instance = new lib.Symbol();
    this.instance.parent = this;
    this.instance.setTransform(7.2, 21, 1, 0.1826, 0, 0, 0, 7.2, 6);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({ _off: false }, 0).to({ scaleY: 1.112, y: 4.35 }, 2).to({ scaleY: 0.9461, y: 6.75 }, 2).to({ scaleY: 1, y: 6 }, 2).wait(107));

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhHAHIAAgNICPAAIAAANg");
    this.shape.setTransform(7.2, 67.6);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AhHAeIAAg7ICPAAIAAA7g");
    this.shape_1.setTransform(7.2, 65.3614);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AhHA0IAAhnICPAAIAABng");
    this.shape_2.setTransform(7.2, 63.1227);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("AhHBLIAAiVICPAAIAACVg");
    this.shape_3.setTransform(7.2, 60.8841);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#FFFFFF").s().p("AhHBhIAAjBICPAAIAADBg");
    this.shape_4.setTransform(7.2, 58.6455);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#FFFFFF").s().p("AhHB3IAAjtICPAAIAADtg");
    this.shape_5.setTransform(7.2, 56.4068);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AhHCOIAAkbICPAAIAAEbg");
    this.shape_6.setTransform(7.2, 54.1682);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#FFFFFF").s().p("AhHCkIAAlHICPAAIAAFHg");
    this.shape_7.setTransform(7.2, 51.9295);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#FFFFFF").s().p("AhHC7IAAl1ICPAAIAAF1g");
    this.shape_8.setTransform(7.2, 49.6909);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#FFFFFF").s().p("AhHDRIAAmhICPAAIAAGhg");
    this.shape_9.setTransform(7.2, 47.4523);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#FFFFFF").s().p("AhHDnIAAnNICPAAIAAHNg");
    this.shape_10.setTransform(7.2, 45.2136);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#FFFFFF").s().p("AhHD+IAAn7ICPAAIAAH7g");
    this.shape_11.setTransform(7.2, 42.975);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#FFFFFF").s().p("AhHD8IAAn3ICPAAIAAH3g");
    this.shape_12.setTransform(7.2, 43.1417);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#FFFFFF").s().p("AhHD6IAAnzICPAAIAAHzg");
    this.shape_13.setTransform(7.2, 43.3083);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#FFFFFF").s().p("AhHD5IAAnxICPAAIAAHxg");
    this.shape_14.setTransform(7.2, 43.475);

    this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.shape }] }).to({ state: [{ t: this.shape_1 }] }, 1).to({ state: [{ t: this.shape_2 }] }, 1).to({ state: [{ t: this.shape_3 }] }, 1).to({ state: [{ t: this.shape_4 }] }, 1).to({ state: [{ t: this.shape_5 }] }, 1).to({ state: [{ t: this.shape_6 }] }, 1).to({ state: [{ t: this.shape_7 }] }, 1).to({ state: [{ t: this.shape_8 }] }, 1).to({ state: [{ t: this.shape_9 }] }, 1).to({ state: [{ t: this.shape_10 }] }, 1).to({ state: [{ t: this.shape_11 }] }, 1).to({ state: [{ t: this.shape_12 }] }, 1).to({ state: [{ t: this.shape_13 }] }, 1).to({ state: [{ t: this.shape_14 }] }, 1).wait(110));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, -2.3, 14.4, 70.7);


  (lib.group = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_2
    this.instance = new lib.Tween1("synched", 0);
    this.instance.parent = this;
    this.instance.setTransform(5.5, 3.95);
    this.instance.alpha = 0;
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({ _off: false }, 0).to({ alpha: 0.6602 }, 4).to({ alpha: 0.25 }, 5).to({ alpha: 0.8203 }, 4).to({ alpha: 0.4609 }, 5).to({ alpha: 1 }, 5).wait(8));

    // Layer_3
    this.instance_1 = new lib.Tween2("synched", 0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(5.6, 13.9);
    this.instance_1.alpha = 0;
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({ _off: false }, 0).to({ alpha: 0.6602 }, 4).to({ alpha: 0.25 }, 5).to({ alpha: 0.8203 }, 4).to({ alpha: 0.4609 }, 5).to({ alpha: 1 }, 5).wait(14));

    // Layer_4
    this.instance_2 = new lib.Tween3("synched", 0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(5.5, 24.4);
    this.instance_2.alpha = 0;
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({ _off: false }, 0).to({ alpha: 0.6602 }, 4).to({ alpha: 0.25 }, 5).to({ alpha: 0.8203 }, 4).to({ alpha: 0.4609 }, 5).to({ alpha: 1 }, 5).wait(12));

    // Layer_5
    this.instance_3 = new lib.Tween4("synched", 0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(5.5, 34.45);
    this.instance_3.alpha = 0;
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({ _off: false }, 0).to({ alpha: 0.6602 }, 4).to({ alpha: 0.25 }, 5).to({ alpha: 0.8203 }, 4).to({ alpha: 0.4609 }, 5).to({ alpha: 1 }, 5).wait(10));

    // Layer_6
    this.instance_4 = new lib.Tween5("synched", 0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(5.5, 45.25);
    this.instance_4.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance_4).to({ alpha: 0.6602 }, 4).to({ alpha: 0.25 }, 5).to({ alpha: 0.8203 }, 4).to({ alpha: 0.4609 }, 5).to({ alpha: 1 }, 5).wait(16));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 11.1, 50.1);


  (lib.Logo_main = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // Layer_2
    this.instance = new lib.A("synched", 0, false);
    this.instance.parent = this;
    this.instance.setTransform(82.7, 42.55, 1, 1, 0, 0, 0, 29.5, 26.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({ startPosition: 14 }, 0).to({ x: 29.5, startPosition: 39 }, 25, cjs.Ease.quadInOut).wait(96).to({ startPosition: 48 }, 0).to({ _off: true }, 1).wait(36));

    // a
    this.instance_1 = new lib.a_("synched", 0, false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(234.9, 42.95, 1, 1, 0, 0, 0, 24.4, 26.2);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(46).to({ _off: false }, 0).wait(89).to({ startPosition: 53 }, 0).to({ _off: true }, 1).wait(32).to({ _off: false }, 0).wait(4));

    // b
    this.instance_2 = new lib.b("synched", 0, false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(182.4, 34.3, 1, 1, 0, 0, 0, 25.1, 34.8);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(43).to({ _off: false }, 0).wait(92).to({ startPosition: 41 }, 0).to({ _off: true }, 1).wait(32).to({ _off: false }, 0).wait(4));

    // i
    this.instance_3 = new lib.i("synched", 0, false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(144.75, 33.6, 1, 1, 0, 0, 0, 7.2, 34.1);
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(48).to({ _off: false }, 0).wait(87).to({ startPosition: 87 }, 0).to({ _off: true }, 1).wait(32).to({ _off: false, startPosition: 120 }, 0).wait(4));

    // R_2
    this.instance_4 = new lib.R_2("synched", 0, false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(116.7, 42.35, 1, 1, 0, 0, 0, 16.2, 25.6);
    this.instance_4._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(37).to({ _off: false }, 0).wait(98).to({ startPosition: 39 }, 0).to({ _off: true }, 1).wait(32).to({ _off: false }, 0).to({ _off: true }, 1).wait(3));

    // R
    this.instance_5 = new lib.R("synched", 0, false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(79.85, 42.35, 1, 1, 0, 0, 0, 16.2, 25.6);
    this.instance_5._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(32).to({ _off: false }, 0).wait(103).to({ startPosition: 96 }, 0).to({ _off: true }, 1).wait(36));

    // Layer_5
    this.instance_6 = new lib.group("single", 38);
    this.instance_6.parent = this;
    this.instance_6.setTransform(265.65, 42.95, 1, 1, 0, 0, 0, 5.5, 25);
    this.instance_6.alpha = 0;
    this.instance_6._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(54).to({ _off: false }, 0).to({ x: 273.3, alpha: 1 }, 24, cjs.Ease.quartOut).wait(57).to({ startPosition: 38 }, 0).to({ _off: true }, 1).wait(36));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, -2.8, 278.9, 81.3);


  // stage content:
  (lib.Arriba_logo = function (mode, startPosition, loop) {
    this.initialize(mode, startPosition, loop, {});

    // timeline functions:
    this.frame_0 = function () {
      this.animation = true;
    }
    this.frame_99 = function () {
      this.stop();
      this.animation = false;
      animationEnded();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(99).call(this.frame_99).wait(37));

    // Layer_1
    this.instance = new lib.Logo_main("synched", 0, false);
    this.instance.parent = this;
    this.instance.setTransform(87.4, 226.6, 8.1968, 8.1968, 0, 0, 0, 27.4, 43);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({ startPosition: 14 }, 0).wait(1).to({ regX: 139.4, regY: 33.2, scaleX: 8.1835, scaleY: 8.1835, x: 1003.95, y: 146.7, startPosition: 15 }, 0).wait(1).to({ scaleX: 8.1598, scaleY: 8.1598, x: 1001.35, y: 147.4, startPosition: 16 }, 0).wait(1).to({ scaleX: 8.1243, scaleY: 8.1243, x: 997.5, y: 148.5, startPosition: 17 }, 0).wait(1).to({ scaleX: 8.0755, scaleY: 8.0755, x: 992.05, y: 149.95, startPosition: 18 }, 0).wait(1).to({ scaleX: 8.0111, scaleY: 8.0111, x: 985, y: 151.85, startPosition: 19 }, 0).wait(1).to({ scaleX: 7.9287, scaleY: 7.9287, x: 975.9, y: 154.35, startPosition: 20 }, 0).wait(1).to({ scaleX: 7.8253, scaleY: 7.8253, x: 964.55, y: 157.4, startPosition: 21 }, 0).wait(1).to({ scaleX: 7.6969, scaleY: 7.6969, x: 950.4, y: 161.25, startPosition: 22 }, 0).wait(1).to({ scaleX: 7.5391, scaleY: 7.5391, x: 933, y: 165.95, startPosition: 23 }, 0).wait(1).to({ scaleX: 7.3464, scaleY: 7.3464, x: 911.8, y: 171.75, startPosition: 24 }, 0).wait(1).to({ scaleX: 7.1126, scaleY: 7.1126, x: 886.05, y: 178.75, startPosition: 25 }, 0).wait(1).to({ scaleX: 6.8324, scaleY: 6.8324, x: 855.2, y: 187.1, startPosition: 26 }, 0).wait(1).to({ scaleX: 6.503, scaleY: 6.503, x: 818.85, y: 196.95, startPosition: 27 }, 0).wait(1).to({ scaleX: 6.1286, scaleY: 6.1286, x: 777.6, y: 208.1, startPosition: 28 }, 0).wait(1).to({ scaleX: 5.7239, scaleY: 5.7239, x: 733.05, y: 220.2, startPosition: 29 }, 0).wait(1).to({ scaleX: 5.3122, scaleY: 5.3122, x: 687.7, y: 232.5, startPosition: 30 }, 0).wait(1).to({ scaleX: 4.9173, scaleY: 4.9173, x: 644.2, y: 244.3, startPosition: 31 }, 0).wait(1).to({ scaleX: 4.5546, scaleY: 4.5546, x: 604.25, y: 255.15, startPosition: 32 }, 0).wait(1).to({ scaleX: 4.2304, scaleY: 4.2304, x: 568.5, y: 264.85, startPosition: 33 }, 0).wait(1).to({ scaleX: 3.9443, scaleY: 3.9443, x: 537.05, y: 273.4, startPosition: 34 }, 0).wait(1).to({ scaleX: 3.6933, scaleY: 3.6933, x: 509.4, y: 280.9, startPosition: 35 }, 0).wait(1).to({ scaleX: 3.4731, scaleY: 3.4731, x: 485.05, y: 287.5, startPosition: 36 }, 0).wait(1).to({ scaleX: 3.2797, scaleY: 3.2797, x: 463.75, y: 293.3, startPosition: 37 }, 0).wait(1).to({ scaleX: 3.1096, scaleY: 3.1096, x: 445, y: 298.35, startPosition: 38 }, 0).wait(1).to({ scaleX: 2.9595, scaleY: 2.9595, x: 428.5, y: 302.85, startPosition: 39 }, 0).wait(1).to({ scaleX: 2.827, scaleY: 2.827, x: 413.9, y: 306.8, startPosition: 40 }, 0).wait(1).to({ scaleX: 2.7099, scaleY: 2.7099, x: 401, y: 310.3, startPosition: 41 }, 0).wait(1).to({ scaleX: 2.6063, scaleY: 2.6063, x: 389.55, y: 313.4, startPosition: 42 }, 0).wait(1).to({ scaleX: 2.5148, scaleY: 2.5148, x: 379.5, y: 316.15, startPosition: 43 }, 0).wait(1).to({ scaleX: 2.434, scaleY: 2.434, x: 370.6, y: 318.55, startPosition: 44 }, 0).wait(1).to({ scaleX: 2.3629, scaleY: 2.3629, x: 362.8, y: 320.7, startPosition: 45 }, 0).wait(1).to({ scaleX: 2.3005, scaleY: 2.3005, x: 355.9, y: 322.5, startPosition: 46 }, 0).wait(1).to({ scaleX: 2.2461, scaleY: 2.2461, x: 349.9, y: 324.15, startPosition: 47 }, 0).wait(1).to({ scaleX: 2.2027, scaleY: 2.2027, x: 345.1, y: 325.5, startPosition: 48 }, 0).wait(1).to({ regX: 27.4, regY: 43.1, scaleX: 2.1694, scaleY: 2.1694, x: 98.3, y: 347.75, startPosition: 49 }, 0).wait(87));

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(399, 229.1, 383.5, 212.29999999999998);
  // library properties:
  lib.properties = {
    id: 'C9D0F576AD48E74AA8BC85C7FFFB43BB',
    width: 790,
    height: 450,
    fps: 23,
    color: "#E41F1F",
    opacity: 1.00,
    manifest: [],
    preloads: []
  };



  // bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
  p.stop = function (ms) { if (ms) this.seek(ms); this.tickEnabled = false; }
  p.seek = function (ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
  p.getDuration = function () { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

  p.getTimelinePosition = function () { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['C9D0F576AD48E74AA8BC85C7FFFB43BB'] = {
    getStage: function () { return exportRoot.getStage(); },
    getLibrary: function () { return lib; },
    getSpriteSheet: function () { return ss; },
    getImages: function () { return img; }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();
    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        }
        else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        }
        else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        }
        else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw; lastH = ih; lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn;
