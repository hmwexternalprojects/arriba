// window.addEventListener("load", function () {
//   init_logo_animation();
// });

function animationEnded() {
  var elem = document.getElementById("canvas");
  // var event = new CustomEvent("logoAnimationEnd");
  // elem.dispatchEvent(event);
} //function starts after  the logo animation is ended

var canvas,
  stage,
  exportRoot,
  anim_container,
  dom_overlay_container,
  fnStartAnimation;
function init_logo_animation() {
  canvas = document.getElementById("canvas");
  anim_container = document.getElementById("animation_container");
  dom_overlay_container = document.getElementById(
    "dom_overlay_container"
  );
  var comp = AdobeAn.getComposition("C9D0F576AD48E74AA8BC85C7FFFB43BB");
  var lib = comp.getLibrary();
  handleComplete({}, comp);
}
function handleComplete(evt, comp) {
  //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
  var lib = comp.getLibrary();
  var ss = comp.getSpriteSheet();
  exportRoot = new lib.Arriba_logo();
  stage = new lib.Stage(canvas);
  //Registers the "tick" event listener.
  fnStartAnimation = function () {
    stage.addChild(exportRoot);
    createjs.Ticker.setFPS(lib.properties.fps);
    createjs.Ticker.addEventListener("tick", stage);
  };
  //Code to support hidpi screens and responsive scaling.
  AdobeAn.makeResponsive(false, "both", false, 1, [
    canvas,
    anim_container,
    dom_overlay_container
  ]);
  AdobeAn.compositionLoaded(lib.properties.id);
  fnStartAnimation();
}
