<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5RRLCZF');</script>
    <!-- End Google Tag Manager -->
  <?php wp_head(); ?>
</head>

<?php 
	$builderClass = '';
	if (function_exists('get_field')) {
		if (get_field('page_builder_enabled', get_the_ID())) {
			$builderClass = 'page-builder-enabled';
		}
	}
?>

<body <?php body_class([$builderClass, 'sticky', basename(get_permalink())]); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RRLCZF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<a href="#" class="mobileToggle"></a>
  <!-- Mobile Menu -->
  <div class="mobile-menu">
    <div id="main-nav">
      <nav id="top-menu-nav">
        <?php
			echo wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'main-menu-nav', 'menu_id' => 'main-menu', 'echo' => false ) );
		?>
      </nav>
    </div> <!-- #main-nav -->
    <?php echo do_shortcode('[social-links-text]'); ?>
  </div>
  <?php wp_body_open(); ?> <a class="skip-link screen-reader-text"
    href="#content"><?php esc_html_e( 'Skip to content', 'hmw' ); ?></a>
  <div id="page" class="site">
    <header id="main-header" class="sticky">
      <div class="header-container clearfix">
        <div class="logo_container">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <?php 
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$logo = wp_get_attachment_image_url( $custom_logo_id, 'full');
				if ( has_custom_logo() ) {
					echo sprintf('<img src="%s" alt="%s" id="logo" />', $logo, get_bloginfo( 'name' ));
				} else {
					echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
				}
			?>
          </a>
        </div>


      </div> <!-- .container -->
    </header> <!-- #main-header -->
    <?php if(is_front_page() && !isset($_COOKIE["header_animation"])) : ?>
    <div id="logo-animation-container">
      <div id="animation_container">
        <canvas id="canvas" style="position: absolute; display: block;"></canvas>
        <div id="dom_overlay_container"
          style="pointer-events:none; overflow:hidden; width:790px; height:450px; position: absolute; left: 0px; top: 0px; display: block;">
        </div>
      </div>
    </div>
    <?php endif; ?>
